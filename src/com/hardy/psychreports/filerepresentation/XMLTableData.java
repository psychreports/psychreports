package com.hardy.psychreports.filerepresentation;

import java.util.List;
import java.util.Map;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import com.hardy.psychreports.filerepresentation.error.XMLObjectCreationException;
import com.hardy.psychreports.filerepresentation.error.XMLObjectUpdateException;
import com.hardy.psychreports.filerepresentation.factory.XMLObjectFactory;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 725 Sandhill Ct
 * Lehi, Ut. 84043
 * 
 * Description: Represents table data for display
 * 
 * Author: Hardy Cherry
 **/
public class XMLTableData extends XMLObject {

	public static final String TABLE_DATA_NODE_NAME = "td";
	
	private XMLObject data;

	public XMLTableData() {

		super();
		data = null;
	}

	public XMLObject getData() {

		return data;
	}

	public void setData(XMLObject data) {

		this.data = data;
	}
	
	@Override
	public void parseXML(Node xml, List<String> errors, Map<String, List<String>> tagsAndIds) {

		if (TABLE_DATA_NODE_NAME.equals(xml.getNodeName())) {
		
			XMLObject n = null;
			try {
				
				Node node = xml.getFirstChild();
				String nodeName = "";
				while(n == null) {
					
					nodeName = node.getNodeName();
					n = XMLObjectFactory.getInstance(nodeName);
					
					if(n == null) {
						node = node.getNextSibling();
					}
				}
				
				n.parseXML(node, errors, tagsAndIds);
				setData(n); 
			} catch(XMLObjectCreationException e) {
				
				errors.add(e.getMessage());
			}
			
			NamedNodeMap attrs = xml.getAttributes();
			if(attrs != null) {
				
				try {
					
					setId(attrs.getNamedItem(ID_ATTR).getNodeValue());					
				} catch(Exception e) { 
					
					errors.add(xml.getNodeName() + " : " + e.getMessage());
				}
			}				
		}
	}

	@Override
	public String toXML() {

		StringBuilder result = new StringBuilder();
		result.append("<")
			.append(TABLE_DATA_NODE_NAME)
			.append(" ")
			.append(ID_ATTR)
			.append("=\"")
			.append(getId())
			.append("\">")
			.append(getData().toXML())
			.append("</")
			.append(TABLE_DATA_NODE_NAME)
			.append(">");
	
		return result.toString();
	}

	@Override
	public javafx.scene.Node createSeceneNode() {

		javafx.scene.Node node = null;
		if(getData() != null) {
			
			node = getData().createSeceneNode();
		}
		
		return node;
	}
	
	@Override
	public void clear() {
		
		if(data != null) {
			
			data.clear();
		}
	}

	@Override
	public String getNodeName() {

		return TABLE_DATA_NODE_NAME;
	}
	
	@Override
	public void update(Object newValue) throws XMLObjectUpdateException {
	
		if(newValue instanceof XMLObject) {
			
			setData((XMLObject)newValue);
		} else {
			
			throw new XMLObjectUpdateException("Invalid type to update TableData with.");
		}
	}

	@Override
	public void replaceTags(Map<String, String> tagsAndReplacement) {

		// TODO Auto-generated method stub		
	}
}
