package com.hardy.psychreports.controllers;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 725 Sandhill Ct
 * Lehi, Ut. 84043
 * 
 * Description: Tracks the current state of the application, this makes it possible to change views without having to do 
 * the logic in the change view process to determine wether the user is going to be editing or creating 
 * a new report
 * 
 * Author: Hardy Cherry
 **/
public class State {

	
	private static States state = States.NONE;
	
	/**
	 * Use this to set the state of the app.
	 * @param newState the new state.
	 */
	public static void setState(States newState) {
		
		state = newState;
	}
	
	/**
	 * @return the current state the app should be in.
	 */
	public static States getState() {
		
		return state;
	}
}
