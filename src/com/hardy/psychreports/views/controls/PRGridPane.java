package com.hardy.psychreports.views.controls;

import javafx.scene.Node;
import javafx.scene.layout.GridPane;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 725 Sandhill Ct
 * Lehi, Ut. 84043
 * 
 * Description:
 * 
 * Author: Hardy Cherry
 **/
public class PRGridPane extends GridPane {

	private int rowCount;
	private int columnCount;
	
	public PRGridPane() {
		
		rowCount = 0;
		columnCount = 0;
	}
	
	public void add(Node child) {
		
		super.add(child, columnCount, rowCount);
	}
	
	public void add(Node child, int column) {
		
		super.add(child, column, rowCount);
	}
	
	public void incrementRow() {
		
		rowCount++;
	}
	
	public int getRowCount() {
		
		return rowCount;
	}
	
	public void incrementColumn() {
		
		columnCount++;
	}
	
	public void resetColumn() {
		
		columnCount = 0;
	}	
}
