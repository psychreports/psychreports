package com.hardy.psychreports.controllers.controls;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.log4j.Logger;

import com.hardy.psychreports.daos.ReportModel;
import com.hardy.psychreports.filerepresentation.XMLObject;
import com.hardy.psychreports.filerepresentation.error.XMLObjectUpdateException;
import com.hardy.psychreports.utils.PsychReportUtils;

import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.web.HTMLEditor;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 725 Sandhill Ct
 * Lehi, Ut. 84043
 * 
 * Description:
 * 
 * Author: Hardy Cherry
 **/
public class TextFieldController {

	private static Logger log = Logger.getLogger(TextFieldController.class);
	
	public static void updateModel(Object source) {
		
		if(source instanceof TextField ||
				source instanceof TextArea || 
				source instanceof HTMLEditor) {
			
			String id = "";
			String newText = "";
			
			if(source instanceof TextField) {
				
				TextField textField = (TextField)source;
				id = textField.getId();
				newText = textField.getText();
			} else if(source instanceof HTMLEditor) {
				
				HTMLEditor htmlEditor = (HTMLEditor)source;
				id = htmlEditor.getId();
				newText = htmlEditor.getHtmlText();
				newText = StringEscapeUtils.escapeXml(newText);
			} else {
				
				TextArea textArea = (TextArea)source;
				id = textArea.getId();
				newText = textArea.getText();
			}
			
			XMLObject xmlObj = ReportModel.getInstance().getXMLFile().getItemById(id);
			
			try {
				
				xmlObj.update(newText);
			} catch(XMLObjectUpdateException e) {
				
				log.error("Unable to update textfield: " + PsychReportUtils.getStackTrace(e));
			}
		}		
	}
}
