package com.hardy.psychreports.controllers.controls;

import java.text.ParseException;
import java.util.Date;

import org.apache.log4j.Logger;

import com.hardy.psychreports.daos.ReportModel;
import com.hardy.psychreports.filerepresentation.XMLObject;
import com.hardy.psychreports.filerepresentation.error.XMLObjectUpdateException;
import com.hardy.psychreports.utils.PsychReportUtils;

import javafx.scene.control.TextField;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 725 Sandhill Ct
 * Lehi, Ut. 84043
 * 
 * Description:
 * 
 * Author: Hardy Cherry
 **/
public class DateFieldController {

	private static Logger log = Logger.getLogger(DateFieldController.class);
	
	private static Date validDateField(Object source) {
		
		Date result = null;
		
		if(source instanceof TextField) {
			
			TextField textField = (TextField)source;
			
			String dateStr = textField.getText();
			
			try {
				
				result = PsychReportUtils.convertStringToDate(dateStr);
			} catch(ParseException e) {

				result = null;
			}
		}
		
		return result;
	}
	
	public static void updateDateField(Object source) {
		
		Date date = validDateField(source);
		
		if(source instanceof TextField) {
			
			TextField textField = (TextField)source;
			
			String id = textField.getId();
			
			XMLObject xmlObject = ReportModel.getInstance().getXMLFile().getItemById(id);
			
			try {
			
				xmlObject.update(date);
			} catch(XMLObjectUpdateException e) {
				
				log.error("Unable to update Date Field: " + PsychReportUtils.getStackTrace(e));
			}
		}	
	}
}
