package com.hardy.psychreports.controllers;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 725 Sandhill Ct
 * Lehi, Ut. 84043
 * 
 * Description: Defines the names of various views and the locations of those views.
 * 
 * Author: Hardy Cherry
 **/
public class Views {


	public static final String START_SCREEN_NAME = "startScreen";
	public static final String NEW_EDIT_SCREEN_NAME = "newEditScreen";
	public static final String EDIT_PROPERTIES_SCREEN_NAME = "editPropertiesScreen";
	public static final String SEARCH_SCREEN_NAME = "searchScreen";
	public static final String REPORT_CREATOR_SCREEN_NAME = "reportCreatorScreen";
	public static final String ABOUT_SCREEN_NAME = "aboutScreen";
	public static final String EDIT_TAGS_NAME = "editTagsScreen";
		
	private static final String START_SCREEN_VIEW = "views/StartScreen.fxml";
	private static final String NEW_EDIT_VIEW = "views/NewEditReport.fxml";
	private static final String EDIT_PROPERTIES_VIEW = "views/EditProperties.fxml";
	private static final String SEARCH_VIEW = "views/SearchReports.fxml";
	private static final String REPORT_CREATOR_VIEW = "views/ReportCreator.fxml";
	private static final String ABOUT_VIEW = "views/AboutScreen.fxml";
	private static final String EDIT_TAGS_VIEW = "views/EditTags.fxml";
	
	/**
	 * Pass in one of the public static strings from this class and 
	 * you will get back the file location of the fxml file.
	 * @param view the view to look up.
	 * @return the corresponding location of that view.
	 */
	public static String getView(String view) {
		
		String result = "";
		
		switch(view) {
			
			case START_SCREEN_NAME:
				result = START_SCREEN_VIEW;
				break;
			case NEW_EDIT_SCREEN_NAME:
				result = NEW_EDIT_VIEW;
				break;
			case EDIT_PROPERTIES_SCREEN_NAME:
				result = EDIT_PROPERTIES_VIEW;
				break;
			case SEARCH_SCREEN_NAME:
				result = SEARCH_VIEW;
				break;
			case REPORT_CREATOR_SCREEN_NAME:
				result = REPORT_CREATOR_VIEW;
				break;
			case ABOUT_SCREEN_NAME:
				result = ABOUT_VIEW;
				break;
			case EDIT_TAGS_NAME:
				result = EDIT_TAGS_VIEW;
				break;
			default:
				result = START_SCREEN_VIEW;
				break;
		}
		
		return result;
	}
}

