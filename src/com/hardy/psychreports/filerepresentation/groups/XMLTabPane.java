package com.hardy.psychreports.filerepresentation.groups;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.AnchorPane;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import com.hardy.psychreports.controllers.ViewChanger;
import com.hardy.psychreports.filerepresentation.XMLDropDown;
import com.hardy.psychreports.filerepresentation.XMLObject;
import com.hardy.psychreports.filerepresentation.XMLOption;
import com.hardy.psychreports.filerepresentation.XMLTableData;
import com.hardy.psychreports.filerepresentation.error.XMLObjectUpdateException;
import com.hardy.psychreports.filerepresentation.groups.XMLTab;
import com.hardy.psychreports.filerepresentation.XMLTable;
import com.hardy.psychreports.filerepresentation.XMLTableRow;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 725 Sandhill Ct
 * Lehi, Ut. 84043
 * 
 * Description: Represents a group of items to be displayed
 *  on a tab pane.
 * 
 * Author: Hardy Cherry
 **/
public class XMLTabPane extends XMLObject {

	public static final String TABPANE_NODE_NAME = "TabPane";
	public static final String INCLUDE_IN_REPORT_ATTR = "includeInReport";
	
	protected List<String> itemNames;
	protected Map<String, XMLTab> tabs;
	protected String includeInReport;

	public XMLTabPane() {

		super();
		itemNames = new ArrayList<>();
		tabs = new HashMap<>();
		includeInReport = "";
	}

	public XMLObject getItemById(String itemId) {
		
		XMLObject result = null;
		
		if(tabs.containsKey(itemId)) {
			
			result = tabs.get(itemId);
		} else {
			
			//search all children groups for the id
			Iterator<? extends XMLObject> itemIter = tabs.values().iterator();
			while(itemIter.hasNext() && result == null) {
				
				XMLObject xmlObj = itemIter.next();
				if(xmlObj instanceof XMLGroup) {
					
					result = ((XMLGroup)xmlObj).getItemById(itemId);
				} else if(xmlObj instanceof XMLDropDown) {
					
					result = ((XMLDropDown)xmlObj).getItemById(itemId);
				} else if(xmlObj instanceof XMLTable) {
					
					result = ((XMLTable)xmlObj).getItemById(itemId);
				} else if(xmlObj instanceof XMLTableRow) {
					
					result = ((XMLTableRow)xmlObj).getItemById(itemId);
				} else if(xmlObj instanceof XMLTabPane) {
					
					result = ((XMLTabPane)xmlObj).getItemById(itemId);
				} 	
			}
		}
		
		return result;
	}
	
	public boolean replaceItem(String itemId, XMLObject xmlObj) {
		
		boolean replacedObject = false;
		
		if(tabs.containsKey(itemId)) {
			
			tabs.remove(itemId);
			tabs.put(itemId, (XMLTab)xmlObj);
			replacedObject = true;
		} else {
		
			//search all children groups for the id
			Iterator<? extends XMLObject> itemIter = tabs.values().iterator();
			while(itemIter.hasNext() && !replacedObject) {
				
				XMLObject tempObj = itemIter.next();
				if(tempObj instanceof XMLGroup) {
					
					replacedObject = ((XMLGroup)tempObj).replaceItem(itemId, xmlObj);
				} else if(tempObj instanceof XMLDropDown) {
					
					if(xmlObj instanceof XMLOption) {
						
						replacedObject = ((XMLDropDown)tempObj).replaceItem(itemId, (XMLOption)xmlObj);
					}
				} else if(tempObj instanceof XMLTable) {
					
					if(xmlObj instanceof XMLTableRow) {
						
						replacedObject = ((XMLTable)tempObj).replaceItem(itemId, (XMLTableRow)xmlObj);
					}
				} else if(tempObj instanceof XMLTableRow) {
					
					if(xmlObj instanceof XMLTableData) {
						
						replacedObject = ((XMLTableRow)tempObj).replaceItem(itemId, (XMLTableData)xmlObj);
					}
				} else if(tempObj instanceof XMLTabPane) {
					
					replacedObject = ((XMLTabPane)tempObj).replaceItem(itemId, xmlObj);
				} 	
			}
		}
		
		return replacedObject;
	}
	
	public List<String> getItemNames() {

		return itemNames;
	}

	public void setItemNames(List<String> itemNames) {

		this.itemNames = itemNames;
	}

	public Map<String, XMLTab> getTabs() {

		return tabs;
	}

	public void setItems(Map<String, XMLTab> tabs) {

		this.tabs = tabs;
	}
	
	public String getIncludeInReport() {
		
		return includeInReport;
	}
	
	public void setIncludeInReport(String includeInReport) {
		
		this.includeInReport = includeInReport;
	}
	
	@Override
	public void parseXML(Node xml, List<String> errors, Map<String, List<String>> tagsAndIds) {
		
		if (TABPANE_NODE_NAME.equals(xml.getNodeName())) {

			NamedNodeMap attrs = xml.getAttributes();
			if (attrs != null) {

				setId(attrs.getNamedItem(ID_ATTR).getNodeValue());	
				setIncludeInReport(attrs.getNamedItem(INCLUDE_IN_REPORT_ATTR).getNodeValue());
			}
			
			for(int i = 0; i < xml.getChildNodes().getLength(); i++) {
				
				Node n = xml.getChildNodes().item(i);
				
				XMLTab xmlTab = new XMLTab();
				xmlTab.parseXML(n, errors, tagsAndIds);
				if(!"".equals(xmlTab.getId())) {
					itemNames.add(xmlTab.getId());
					tabs.put(xmlTab.getId(), xmlTab);
				}
			}
		}
	}

	@Override
	public String toXML() {
		
		StringBuilder result = new StringBuilder();
		result.append("<")
			.append(TABPANE_NODE_NAME)
			.append(" ")
			.append(ID_ATTR)
			.append("=\"")
			.append(getId())
			.append("\" ")
			.append(INCLUDE_IN_REPORT_ATTR)
			.append("=\"")
			.append(getIncludeInReport())
			.append("\">");
		
		for (int i = 0; i < itemNames.size(); i++) {

			XMLTab xmlTab = tabs.get(itemNames.get(i));
			result.append(xmlTab.toXML());
		}
		
		result.append("</")
			.append(TABPANE_NODE_NAME)
			.append(">");

		return result.toString();
	}

	@Override
	public javafx.scene.Node createSeceneNode() {
	
		AnchorPane pane = new AnchorPane();
		
		TabPane node = new TabPane();
		node.setMinSize(ViewChanger.DEFAULT_WIDTH, ViewChanger.DEFAULT_HEIGHT);
		node.setMaxSize(ViewChanger.DEFAULT_WIDTH, ViewChanger.DEFAULT_HEIGHT);
		node.setManaged(true);
		node.setId(getId());
		node.getTabs().clear();
		
		for(int i = 0; i < itemNames.size(); i++) {
			
			XMLTab xmlTab = tabs.get(itemNames.get(i));
			Tab child = xmlTab.createTab();
			node.getTabs().add(child);
		}
		
		pane.getChildren().add(node);
		AnchorPane.setBottomAnchor(node, 0.0);
		AnchorPane.setLeftAnchor(node, 0.0);
		AnchorPane.setRightAnchor(node, 0.0);
		AnchorPane.setTopAnchor(node, 0.0);
		
		return pane;
	}
	
	@Override 
	public void clear() {
		
		if(tabs != null && itemNames != null) {
			
			Iterator<String> itemNameIter = itemNames.iterator();
			while(itemNameIter.hasNext()) {
				
				String name = itemNameIter.next();
				XMLTab tempTab = tabs.get(name);
				if(tempTab != null) {
					
					tempTab.clear();
				}
			}
		}
	}

	@Override
	public String getNodeName() {

		return TABPANE_NODE_NAME;
	}
	
	@Override
	public void update(Object newValue) throws XMLObjectUpdateException {
	
		throw new XMLObjectUpdateException("Unable to update XML Object, doesn't support updating.");
	}

	@Override
	public void replaceTags(Map<String, String> tagsAndReplacement) {

		// TODO Auto-generated method stub
	}
}
