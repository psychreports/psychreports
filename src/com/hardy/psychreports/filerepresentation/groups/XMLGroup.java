package com.hardy.psychreports.filerepresentation.groups;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javafx.geometry.Insets;
import javafx.scene.control.Accordion;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import com.hardy.psychreports.controllers.controls.TitlePaneController;
import com.hardy.psychreports.filerepresentation.XMLDropDown;
import com.hardy.psychreports.filerepresentation.XMLObject;
import com.hardy.psychreports.filerepresentation.XMLOption;
import com.hardy.psychreports.filerepresentation.XMLTable;
import com.hardy.psychreports.filerepresentation.XMLTableData;
import com.hardy.psychreports.filerepresentation.XMLTableRow;
import com.hardy.psychreports.filerepresentation.error.XMLObjectCreationException;
import com.hardy.psychreports.filerepresentation.error.XMLObjectUpdateException;
import com.hardy.psychreports.filerepresentation.factory.XMLObjectFactory;
import com.hardy.psychreports.views.controls.PRTitledPane;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 725 Sandhill Ct
 * Lehi, Ut. 84043
 * 
 * Description: A group of items for display. No special formatting is applied
 *              to the items in the group. Just an ordering based on position in
 *              the group.
 * 
 * Author: Hardy Cherry
 **/
public class XMLGroup extends XMLObject {

	public static final String GROUP_NODE_NAME = "Group";
	public static final String INCLUDE_IN_REPORT_ATTR = "includeInReport";

	protected List<String> itemNames;
	protected Map<String, XMLObject> items;
	protected String includeInReport;
	
	private Accordion accordion;
	private PRTitledPane titlePane;

	public XMLGroup() {

		super();
		itemNames = new ArrayList<>();
		items = new HashMap<>();
		includeInReport = "true";
	}

	public XMLObject getItemById(String itemId) {

		XMLObject result = null;

		if (items.containsKey(itemId)) {

			result = items.get(itemId);
		} else {

			// search all children groups for the id
			Iterator<? extends XMLObject> itemIter = items.values().iterator();
			while (itemIter.hasNext() && result == null) {

				XMLObject xmlObj = itemIter.next();
				if(xmlObj instanceof XMLGroup) {
					
					result = ((XMLGroup)xmlObj).getItemById(itemId);
				} else if(xmlObj instanceof XMLDropDown) {
					
					result = ((XMLDropDown)xmlObj).getItemById(itemId);
				} else if(xmlObj instanceof XMLTable) {
					
					result = ((XMLTable)xmlObj).getItemById(itemId);
				} else if(xmlObj instanceof XMLTableRow) {
					
					result = ((XMLTableRow)xmlObj).getItemById(itemId);
				} else if(xmlObj instanceof XMLTabPane) {
					
					result = ((XMLTabPane)xmlObj).getItemById(itemId);
				} 	
			}
		}

		return result;
	}

	public boolean replaceItem(String itemId, XMLObject xmlObj) {
		
		boolean replacedObject = false;
		
		if(items.containsKey(itemId)) {
			
			items.remove(itemId);
			items.put(itemId, xmlObj);
			replacedObject = true;
		} else {
		
			//search all children groups for the id
			Iterator<? extends XMLObject> itemIter = items.values().iterator();
			while(itemIter.hasNext() && !replacedObject) {
				
				XMLObject tempObj = itemIter.next();
				if(tempObj instanceof XMLGroup) {
					
					replacedObject = ((XMLGroup)tempObj).replaceItem(itemId, xmlObj);
				} else if(tempObj instanceof XMLDropDown) {
					
					if(xmlObj instanceof XMLOption) {
						
						replacedObject = ((XMLDropDown)tempObj).replaceItem(itemId, (XMLOption)xmlObj);
					}
				} else if(tempObj instanceof XMLTable) {
					
					if(xmlObj instanceof XMLTableRow) {
						
						replacedObject = ((XMLTable)tempObj).replaceItem(itemId, (XMLTableRow)xmlObj);
					}
				} else if(tempObj instanceof XMLTableRow) {
					
					if(xmlObj instanceof XMLTableData) {
						
						replacedObject = ((XMLTableRow)tempObj).replaceItem(itemId, (XMLTableData)xmlObj);
					}
				} else if(tempObj instanceof XMLTabPane) {
					
					replacedObject = ((XMLTabPane)tempObj).replaceItem(itemId, xmlObj);
				} 	
			}
		}
		
		return replacedObject;
	}

	public List<String> getItemNames() {

		return itemNames;
	}

	public void setItemNames(List<String> itemNames) {

		this.itemNames = itemNames;
	}

	public Map<String, XMLObject> getItems() {

		return items;
	}

	public void setItems(Map<String, XMLObject> items) {

		this.items = items;
	}

	public String getIncludeInReport() {

		return includeInReport;
	}

	public void setIncludeInReport(String includeInReport) {

		this.includeInReport = includeInReport;
	}

	@Override
	public void parseXML(Node xml, List<String> errors, Map<String, List<String>> tagsAndIds) {

		parseXML(xml, errors, GROUP_NODE_NAME, tagsAndIds);
	}

	protected void parseXML(Node xml, List<String> errors, String nodeName, Map<String, List<String>> tagsAndIds) {

		if (nodeName.equals(xml.getNodeName())) {

			NamedNodeMap attrs = xml.getAttributes();
			if (attrs != null) {

				setId(attrs.getNamedItem(ID_ATTR).getNodeValue());
				setIncludeInReport(attrs.getNamedItem(INCLUDE_IN_REPORT_ATTR).getNodeValue());
			}

			for (int i = 0; i < xml.getChildNodes().getLength(); i++) {

				Node n = xml.getChildNodes().item(i);

				try {
					XMLObject xmlObj = XMLObjectFactory.getInstance(n.getNodeName());
					if (xmlObj != null) {

						xmlObj.parseXML(n, errors, tagsAndIds);
						itemNames.add(xmlObj.getId());
						items.put(xmlObj.getId(), xmlObj);
					}
				} catch (XMLObjectCreationException e) {

					errors.add(n.getNodeName() + " : " + e.getMessage());
				}
			}
		}
	}

	@Override
	public String toXML() {

		return toXML(GROUP_NODE_NAME);
	}

	protected String toXML(String nodeName) {

		StringBuilder result = new StringBuilder();
		result.append("<")
		.append(nodeName)
		.append(" ")
		.append(ID_ATTR)
		.append("=\"")
		.append(getId())
		.append("\" ")
		.append(INCLUDE_IN_REPORT_ATTR)
		.append("=\"")
		.append(getIncludeInReport())
		.append("\">");

		for (int i = 0; i < itemNames.size(); i++) {

			XMLObject obj = items.get(itemNames.get(i));
			result.append(obj.toXML());
		}

		result.append("</").append(nodeName).append(">");

		return result.toString();
	}

	@Override
	public javafx.scene.Node createSeceneNode() {

		AnchorPane anchorpane = new AnchorPane();

		titlePane = new PRTitledPane();

		if(includeInReport.equalsIgnoreCase("false")) {
			
			titlePane.setText("Not included in Report");
		} else {
			
			titlePane.setText("Included in Report");
		}
		titlePane.setId(getId());
		String includeInReport = getIncludeInReport();
		titlePane.setExpanded(!includeInReport.equalsIgnoreCase("false"));
		titlePane.setOnMouseClicked(TitlePaneController.includeBoxClickedEventHandler);

		VBox vBox = new VBox();
		vBox.setManaged(true);
		vBox.setId(getId());

		for (int i = 0; i < itemNames.size(); i++) {

			XMLObject xmlObj = items.get(itemNames.get(i));
			javafx.scene.Node child = xmlObj.createSeceneNode();
			vBox.getChildren().add(child);
		}

		titlePane.setContent(vBox);

		accordion = new Accordion();
		titlePane.setParentAccordion(accordion);
		accordion.getPanes().add(titlePane);
		if (titlePane.isExpanded()) {
			accordion.setExpandedPane(titlePane);
		}
		accordion.expandedPaneProperty().addListener(TitlePaneController.changeListener);

		anchorpane.getChildren().addAll(accordion); // Add grid from Example 1-5
		AnchorPane.setBottomAnchor(accordion, 0.0);
		AnchorPane.setLeftAnchor(accordion, 0.0);
		AnchorPane.setRightAnchor(accordion, 0.0);
		AnchorPane.setTopAnchor(accordion, 0.0);

		anchorpane.setPadding(new Insets(5, 0, 5, 0));

		return anchorpane;
	}
	
	@Override 
	public void clear() {
		
		if(items != null && itemNames != null) {
			
			Iterator<String> itemNameIter = itemNames.iterator();
			while(itemNameIter.hasNext()) {
				
				String name = itemNameIter.next();
				XMLObject tempObj = items.get(name);
				if(tempObj != null) {
					
					tempObj.clear();
				}
			}
		}
		
		if(titlePane != null) {
		
			titlePane.removeEventHandler(MouseEvent.MOUSE_CLICKED, TitlePaneController.includeBoxClickedEventHandler);
		}
		
		if(accordion != null) {
			
			accordion.expandedPaneProperty().removeListener(TitlePaneController.changeListener);
		}
	}

	@Override
	public String getNodeName() {

		return GROUP_NODE_NAME;
	}
	
	@Override
	public void update(Object newValue) throws XMLObjectUpdateException {
	
		if(newValue instanceof Boolean) {
			
			setIncludeInReport(((Boolean)newValue).toString().toLowerCase());
		} else {
			
			throw new XMLObjectUpdateException("Invalid type to update Group with.");
		}
	}

	@Override
	public void replaceTags(Map<String, String> tagsAndReplacement) {

		// TODO Auto-generated method stub		
	}
}
