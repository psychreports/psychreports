package com.hardy.psychreports.views.controls;

import javafx.scene.control.Accordion;
import javafx.scene.control.TitledPane;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 725 Sandhill Ct
 * Lehi, Ut. 84043
 * 
 * Description:
 * 
 * Author: Hardy Cherry
 **/
public class PRTitledPane extends TitledPane {

	private Accordion parentAccordion;
	
	public PRTitledPane() {
		
		super();
	}
	
	public void setParentAccordion(Accordion accordion) {
		
		parentAccordion = accordion;
	}
	
	public Accordion getParentAccordion() {
		
		return parentAccordion;
	}
}
