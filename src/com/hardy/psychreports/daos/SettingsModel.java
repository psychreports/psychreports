package com.hardy.psychreports.daos;

import java.io.File;
import java.text.ParseException;

import org.apache.log4j.Logger;

import com.hardy.psychreports.dom.SettingsDom;
import com.hardy.psychreports.utils.PsychReportUtils;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 725 Sandhill Ct
 * Lehi, Ut. 84043
 * 
 * Description: The model for settings.
 * 
 * Author: Hardy Cherry
 **/
public class SettingsModel {

	private static Logger log = Logger.getLogger(SettingsModel.class);
	
	private final static String SETTINGS_FILE_NAME = "settings.dat";
	private File f;
	private SettingsDom settings;
	private static SettingsModel model = null;

	public static SettingsModel getInstance() {

		if (model == null) {

			model = new SettingsModel();
		}

		return model;
	}

	private SettingsModel() {

		boolean error = false;
		settings = new SettingsDom();
		f = new File(settings.getSettingsDirectory());
		if(!f.mkdirs()) {
			
			log.error("Unable to create directory: " + settings.getSettingsDirectory());
			error = true;
		}
		
		f = new File(settings.getSettingsDirectory() + File.separator + SETTINGS_FILE_NAME);
		try {

			settings.parse(FileAccess.readFile(f));
		} catch (ParseException e) {

			log.error("Error parsing the settings file. It probably didn't exist: " + PsychReportUtils.getStackTrace(e));
		}
		
		f = new File(settings.getReportDirectory());
		if(!f.mkdirs()) {
			
			log.error("Unable to create directory: " + settings.getReportDirectory());
			error = true;
		}
		
		f = new File(settings.getTemplateDirectory());
		if(!f.mkdirs()) {
			
			log.error("Unable to create directory: " + settings.getTemplateDirectory());
			error = true;
		}
		if(error) {
			error = true;
		}
	}

	/**
	 * @param location
	 *            the new location for saved reports
	 */
	public void setBaseDirectory(String location) {

		settings.setReportDirectory(location);
		settings.setTemplateDirectory(location);
		settings.setSettingsDirectory(location);
		FileAccess.writeFile(f, settings);
	}

	/**
	 * @return the current location for saved reports
	 */
	public String getReportDirectory() {

		return settings.getReportDirectory();
	}
	
	public String getTemplateDirectory() {
		
		return settings.getTemplateDirectory();
	}
	
	public String getSettingsDirectory() {
		
		return settings.getSettingsDirectory();
	}
}
