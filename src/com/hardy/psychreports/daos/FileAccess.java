package com.hardy.psychreports.daos;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.CodeSource;

import javax.xml.stream.Location;

import org.apache.log4j.Logger;

import com.hardy.psychreports.daos.error.FileAccessException;
import com.hardy.psychreports.dom.WritableDom;
import com.hardy.psychreports.utils.PsychReportUtils;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 725 Sandhill Ct
 * Lehi, Ut. 84043
 * 
 * Description: This class contains methods for opening and writing files. .
 * 
 * Author: Hardy Cherry
 **/
public class FileAccess {

	private static Logger log = Logger.getLogger(FileAccess.class);

	/**
	 * Takes a file and reads the contents and returns them as a string.
	 * 
	 * @param f
	 *            the file to read
	 * @return the string contents of the file
	 */
	public static String readFile(File f) {

		FileInputStream fis = null;
		StringBuilder builder = new StringBuilder();
		try {

			fis = new FileInputStream(f);

			byte[] b = new byte[1024];
			int len = 0;
			while ((len = fis.read(b)) != -1) {

				for(int i = 0; i < len; i++) {
				
					builder.append((char)b[i]);
				}
			}
		} catch (FileNotFoundException e) {

			try {

				f.createNewFile();
			} catch (IOException e1) {

				log.error("Unable to create file: " + f.getPath());
			}
		} catch (Exception e) {

			log.error("An error occured while reading the file: " + PsychReportUtils.getStackTrace(e));
		} finally {

			try {

				if (fis != null) {

					fis.close();
				}
			} catch (IOException e) {

				log.error("Error closing the file: " + PsychReportUtils.getStackTrace(e));
			}
		}

		return builder.toString();
	}

	/**
	 * Writes the contents of a WritableDom objects output method to the file
	 * passed in.
	 * 
	 * @param f
	 *            the file to write
	 * @param writableDom
	 *            the dom object containing data to write.
	 */
	public static void writeFile(File f, WritableDom writableDom) {

		FileOutputStream fos = null;
		try {

			fos = new FileOutputStream(f);

			String output = writableDom.output();
			fos.write(output.getBytes());
		} catch (Exception e) {

			log.error("Error writing the file: " + f.getPath() + "\n\tERROR: " + PsychReportUtils.getStackTrace(e));
		} finally {

			try {

				fos.flush();
				fos.close();
			} catch (IOException e) {

				log.error("Error closing the file stream: " + PsychReportUtils.getStackTrace(e));
			}
		}
	}
	
	public static String getRootPath() throws FileAccessException {
		
		String location = "";
		CodeSource cs = Location.class.getProtectionDomain().getCodeSource();
		if (cs != null) {

			location = cs.getLocation().getPath();
		} else {

			log.error("Unable to get the location from the code source.");
			location = System.getProperty("user.dir");

			if (location == null || "".equals(location)) {
				
				log.error("Unable to get the location from System.getProperty");
				location = new java.io.File("").getAbsolutePath().toString();
				
				if(location == null || "".equals(location)) {
					
					log.error("Unable to get the location from file.getAbsolutePath");
				}
			}
		}
		
		if(location == null || "".equals(location)) {
			
			log.error("Unable to get the root location of the app");
			throw new FileAccessException("Unable to get the root location");
		}
		
		return location;
	}
}
