package com.hardy.psychreports.views.controls;

import com.hardy.psychreports.filerepresentation.XMLObject;
import com.hardy.psychreports.filerepresentation.error.XMLObjectCreationException;
import com.hardy.psychreports.filerepresentation.factory.XMLObjectFactory;

import javafx.scene.control.Button;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 725 Sandhill Ct
 * Lehi, Ut. 84043
 * 
 * Description:
 * 
 * Author: Hardy Cherry
 **/
public class PRButton extends Button {

	private String tableDataContentType;
	private PRGridPane tableNode;
	
	public PRButton() {
		
		tableDataContentType = null;
		tableNode = null;
	}
	
	public void setTableDataContentType(String contentType) {
		
		tableDataContentType = contentType;
	}
	
	public XMLObject getNewTableDataContentType() throws XMLObjectCreationException {
		
		return XMLObjectFactory.getInstance(tableDataContentType);
	}
	
	public void setTableNode(PRGridPane tableNode) {
		
		this.tableNode = tableNode;
	}
	
	public PRGridPane getTableNode() {
		
		return tableNode;
	}
}
