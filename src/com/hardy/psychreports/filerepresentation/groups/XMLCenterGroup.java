package com.hardy.psychreports.filerepresentation.groups;

import java.util.List;
import java.util.Map;

import javafx.geometry.Pos;
import javafx.scene.layout.VBox;

import org.w3c.dom.Node;

import com.hardy.psychreports.filerepresentation.XMLObject;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 725 Sandhill Ct
 * Lehi, Ut. 84043
 * 
 * Description: Represents a group of items to be centered.
 * 
 * Author: Hardy Cherry
 **/
public class XMLCenterGroup extends XMLGroup {

	public static final String CENTERGROUP_NODE_NAME = "CenterGroup";
	
	public XMLCenterGroup() {

		super();
	}

	@Override
	public void parseXML(Node xml, List<String> errors, Map<String, List<String>> tagsAndIds) {

		parseXML(xml, errors, CENTERGROUP_NODE_NAME, tagsAndIds);
	}

	@Override
	public String toXML() {

		return toXML(CENTERGROUP_NODE_NAME);
	}
	
	@Override
	public javafx.scene.Node createSeceneNode() {
	
		VBox node = new VBox();
		node.setAlignment(Pos.CENTER);
		node.setManaged(true);
		node.setId(getId());
		
		for(int i = 0; i < itemNames.size(); i++) {
			
			XMLObject xmlObj = items.get(itemNames.get(i));
			javafx.scene.Node child = xmlObj.createSeceneNode();
			node.getChildren().add(child);
		}
		
		return node;
	}
	
	@Override
	public String getNodeName() {

		return CENTERGROUP_NODE_NAME;
	}
}
