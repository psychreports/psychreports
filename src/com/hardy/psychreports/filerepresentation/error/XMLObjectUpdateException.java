package com.hardy.psychreports.filerepresentation.error;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 725 Sandhill Ct
 * Lehi, Ut. 84043
 * 
 * Description:
 * 
 * Author: Hardy Cherry
 **/
public class XMLObjectUpdateException extends Exception {

	/**
	 * default serial id
	 */
	private static final long serialVersionUID = 1L;

	public XMLObjectUpdateException(String message) {
		
		super(message);
	}
}
