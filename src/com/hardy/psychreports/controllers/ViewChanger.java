package com.hardy.psychreports.controllers;

import java.io.IOException;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import com.hardy.psychreports.PsychReports;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 725 Sandhill Ct
 * Lehi, Ut. 84043
 * 
 * Description: This class makes it easy to move from one view to another. 
 * <br />
 * call: ViewChanger.changeView(this, Views.ONE_OF_THE_CONSTANT_STRINGS
 * 
 * Author: Hardy Cherry
 **/
public class ViewChanger {

	public static final Double DEFAULT_WIDTH = 900.0;
	public static final Double DEFAULT_CONTROL_WIDTH = DEFAULT_WIDTH-100;
	public static final Double DEFAULT_HEIGHT = 680.0;
	/**
	 * Changes the visable window.
	 * @param o should almost always be 'this'
	 * @param view Use one of the final static strings in Views
	 * @param width the width you want the new window or -1 for a default size.
	 * @param height the height you want the new window or -1 for a default size.
	 * @throws IOException
	 */
	public static void changeView(String view) throws IOException {
		
		changeView(view, DEFAULT_WIDTH, DEFAULT_HEIGHT);
	}
	
	public static void changeView(String view, Double width, Double height) throws IOException {
				
		Stage primaryStage = PsychReports.getPrimaryStage();
		Scene scene = null;
		
		double w = width;
		double h = height;
		if(w <= 50) {
			
			w = DEFAULT_WIDTH;
		}
		
		if(h <= 50) {
			
			h = DEFAULT_HEIGHT;
		}
		
		Parent root = FXMLLoader.load(PsychReports.class.getResource(Views.getView(view)));
		scene = new Scene(root, w, h);
		
		primaryStage.setTitle("Psych Reports");
		primaryStage.close();
		primaryStage.setScene(scene);
		primaryStage.show();
	}
}
