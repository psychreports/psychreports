package com.hardy.psychreports.controllers;

import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

import com.hardy.psychreports.controllers.controls.LabeledTextField;
import com.hardy.psychreports.daos.ReportModel;
import com.hardy.psychreports.filerepresentation.XMLFile;
import com.hardy.psychreports.utils.PsychReportUtils;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 725 Sandhill Ct
 * Lehi, Ut. 84043
 * 
 * Description:
 * 
 * Author: Hardy Cherry
 **/
public class EditTagsController implements Initializable {

	private static Logger log = Logger.getLogger(EditTagsController.class);
	
	@FXML
	protected ListView<LabeledTextField> tagsView;
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {

		XMLFile xmlFile = ReportModel.getInstance().getXMLFile();
		Map<String, List<String>> tagsAndIds = xmlFile.getTagsAndIds();
		Map<String, String> tagsAndReplacement = xmlFile.getTagsAndReplacements();
		
		tagsView.getItems().clear();
		
		Iterator<String> tagsIter = tagsAndIds.keySet().iterator();
		while(tagsIter.hasNext()) {
			
			String tag = tagsIter.next();
			
			String text = tagsAndReplacement.get(tag);
			
			LabeledTextField tempField = new LabeledTextField(tag);
			tempField.setReplacementText(text);
			tagsView.getItems().add(tempField);
		}
	}

	@FXML
	protected void finish() {
		
		XMLFile xmlFile = ReportModel.getInstance().getXMLFile();
		Map<String, String> tagsAndReplacement = new HashMap<>();
		
		Iterator<LabeledTextField> labelIter = tagsView.getItems().iterator();
		while(labelIter.hasNext()) {
			
			LabeledTextField tempField = labelIter.next();
			String tag = tempField.getTag();
			String replacement = tempField.getReplacementText();
			
			if(replacement != null && !replacement.equals("")) {
				
				tagsAndReplacement.put(tag, replacement);
			}
		}
		
		xmlFile.setTagsAndReplacements(tagsAndReplacement);
		
		try {
			
			State.setState(States.EDIT_PRE_LOADED);
			ViewChanger.changeView(Views.NEW_EDIT_SCREEN_NAME);
		} catch(Exception e) {
			
			log.error("Can't return to new edit screen: " + PsychReportUtils.getStackTrace(e));
		}
			
	}
}
