package com.hardy.psychreports.controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.Initializable;

import org.apache.log4j.Logger;

import javafx.fxml.FXML;
import com.hardy.psychreports.utils.PsychReportUtils;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 725 Sandhill Ct
 * Lehi, Ut. 84043
 * 
 * Description:
 * 
 * Author: Hardy Cherry
 **/
public class AboutScreenController implements Initializable {

	private static Logger log = Logger.getLogger(AboutScreenController.class);
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
	}
	
	@FXML
	protected void close() {
		
		try {
			
			ViewChanger.changeView(Views.START_SCREEN_NAME);
		} catch(IOException e) {
			
			log.error("Can't return to start screen: " + PsychReportUtils.getStackTrace(e));
		}
	}
}
