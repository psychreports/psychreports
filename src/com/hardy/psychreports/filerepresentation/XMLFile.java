package com.hardy.psychreports.filerepresentation;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import com.hardy.psychreports.dom.WritableDom;
import com.hardy.psychreports.dom.ParseableDom;
import com.hardy.psychreports.filerepresentation.error.XMLObjectCreationException;
import com.hardy.psychreports.filerepresentation.factory.XMLObjectFactory;
import com.hardy.psychreports.filerepresentation.groups.XMLGroup;
import com.hardy.psychreports.filerepresentation.groups.XMLTab;
import com.hardy.psychreports.filerepresentation.groups.XMLTabPane;
import com.hardy.psychreports.utils.PsychReportUtils;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 725 Sandhill Ct
 * Lehi, Ut. 84043
 * 
 * Description: represents a file loaded from the filesystem contians all the
 * 
 * Author: Hardy Cherry
 **/
public class XMLFile implements WritableDom, ParseableDom {

	public static final String REPORT_EXT = ".report";
	public static final String TEMPLATE_EXT = ".template";
	
	private static Logger log = Logger.getLogger(XMLFile.class);
	
	public static final String REPORT_NODE = "Report";
	public static final String SETTINGS_NODE = "Settings";
	public static final String SEARCH_INFO_NODE = "SearchInfo";
	public static final String REPORT_SAVED_DATE = "ReportSavedDate";
	public static final String PATIENT_NAME = "PatientName";
	public static final String REPORT_NOTES = "ReportNotes";
	public static final String FIELDS_NODE = "Fields";
	
	
	private List<String> itemIds;
	private Map<String, XMLObject> items;
	private Date reportSavedDate;
	private String patientName;
	private String reportNotes;
	private Map<String, List<String>> tagsAndIds;
	private Map<String, String> tagsAndReplacements;
	
	public XMLFile() {

		reportSavedDate = null;
		itemIds = new ArrayList<>();
		items = new HashMap<String, XMLObject>();
		patientName = "";
		reportNotes = "";
		tagsAndIds = new HashMap<>();
		tagsAndReplacements = new HashMap<>();
	}

	public void clear() {
		
		Iterator<String> itemIdIter = getItemIdIterator();
		while(itemIdIter.hasNext()) {
			
			String id = itemIdIter.next();
			XMLObject tempObj = items.get(id);
			if(tempObj != null) {
				
				tempObj.clear();
			}
		}
		
		itemIds.clear();
		items.clear();
		reportSavedDate = null;
	}
	
	public void addItem(String itemId, XMLObject item) {

		itemIds.add(itemId);
		items.put(itemId, item);
	}

	public void removeItem(String itemId) {

		itemIds.remove(itemId);
		items.remove(itemId);
	}
	
	/**
	 * @param itemId
	 *            the item to look up
	 * @return an xmlObject or null
	 */
	public XMLObject getItemById(String itemId) {

		XMLObject result = null;

		if (items.containsKey(itemId)) {

			result = items.get(itemId);
		} else {
			
			//search all children groups for the id
			Iterator<? extends XMLObject> itemIter = items.values().iterator();
			while(itemIter.hasNext() && result == null) {
				
				XMLObject xmlObj = itemIter.next();
				if(xmlObj instanceof XMLGroup) {
					
					result = ((XMLGroup)xmlObj).getItemById(itemId);
				} else if(xmlObj instanceof XMLDropDown) {
					
					result = ((XMLDropDown)xmlObj).getItemById(itemId);
				} else if(xmlObj instanceof XMLTable) {
					
					result = ((XMLTable)xmlObj).getItemById(itemId);
				} else if(xmlObj instanceof XMLTableRow) {
					
					result = ((XMLTableRow)xmlObj).getItemById(itemId);
				} else if(xmlObj instanceof XMLTabPane) {
					
					result = ((XMLTabPane)xmlObj).getItemById(itemId);
				} 			
			}
		}

		return result;
	}
	
	public boolean replaceItem(String itemId, XMLObject xmlObj) {
		
		boolean replacedObject = false;
		
		if(items.containsKey(itemId)) {
			
			items.remove(itemId);
			items.put(itemId, (XMLTab)xmlObj);
			replacedObject = true;
		} else {
		
			//search all children groups for the id
			Iterator<? extends XMLObject> itemIter = items.values().iterator();
			while(itemIter.hasNext() && !replacedObject) {
				
				XMLObject tempObj = itemIter.next();
				if(tempObj instanceof XMLGroup) {
					
					replacedObject = ((XMLGroup)tempObj).replaceItem(itemId, xmlObj);
				} else if(tempObj instanceof XMLDropDown) {
					
					if(xmlObj instanceof XMLOption) {
						
						replacedObject = ((XMLDropDown)tempObj).replaceItem(itemId, (XMLOption)xmlObj);
					}
				} else if(tempObj instanceof XMLTable) {
					
					if(xmlObj instanceof XMLTableRow) {
						
						replacedObject = ((XMLTable)tempObj).replaceItem(itemId, (XMLTableRow)xmlObj);
					}
				} else if(tempObj instanceof XMLTableRow) {
					
					if(xmlObj instanceof XMLTableData) {
						
						replacedObject = ((XMLTableRow)tempObj).replaceItem(itemId, (XMLTableData)xmlObj);
					}
				} else if(tempObj instanceof XMLTabPane) {
					
					replacedObject = ((XMLTabPane)tempObj).replaceItem(itemId, xmlObj);
				} 	
			}
		}
		
		return replacedObject;
	}
	
	public Iterator<String> getItemIdIterator() {

		return itemIds.iterator();
	}

	public Date getReportSavedDate() {

		if(reportSavedDate == null) {
			reportSavedDate = new Date();
		}
		
		return reportSavedDate;
	}

	public void setReportSavedDate(Date reportSavedDate) {

		this.reportSavedDate = reportSavedDate;
	}
	
	public String getPatientName() {
		
		return patientName;
	}
	
	public void setPatientName(String patientName) {
		
		this.patientName = patientName;
	}
	
	public String getReportNotes() {
		
		return reportNotes;
	}
	
	public void setReportNotes(String reportNotes) {
		
		this.reportNotes = reportNotes;
	}
	public String getSettingsXML() {
		
		return "";
	}
	
	public void parseXML(Node xml, List<String> errors, Map<String, List<String>> tagsAndIdsMap) {
		
		for(int i = 0; i < xml.getChildNodes().getLength(); i++) {
			
			Node childNode = xml.getChildNodes().item(i);
			switch(childNode.getNodeName()) {
				
				case SETTINGS_NODE:
					parseSettings(childNode, errors);
					break;
				case FIELDS_NODE:
					parseFields(childNode, errors, tagsAndIdsMap);
					break;
				case SEARCH_INFO_NODE:
					parseSearchInfo(childNode, errors);
					break;
				case "#text":
					//this covers the case when there are \t, \n or \r to format the xml.
					break;
				default:
					errors.add("Unable to parse node: " + childNode.getNodeName());
			}
		}
	}
	
	private void parseSettings(Node xml, List<String> errors) {
		
	}
	
	private void parseFields(Node xml, List<String> errors, Map<String, List<String>> tagsAndIdsMap) {
		
		for(int i = 0; i < xml.getChildNodes().getLength(); i++) {
		
			try {
				
				Node childNode = xml.getChildNodes().item(i);
				XMLObject obj = XMLObjectFactory.getInstance(childNode.getNodeName());
				if(obj != null) {
				
					obj.parseXML(childNode, errors, tagsAndIdsMap);
					itemIds.add(obj.getId());
					items.put(obj.getId(), obj);
				}
			} catch(XMLObjectCreationException e) {
				
				errors.add(xml.getNodeName() + " : " + e.getMessage());
			}
		}			
	}
	
	private void parseSearchInfo(Node xml, List<String> errors) {
		
		SimpleDateFormat format = new SimpleDateFormat("EEEE MMM dd hh:mm:ss z yyyy");
		
		for(int i = 0; i < xml.getChildNodes().getLength(); i++) {
			
			try {
				Node childNode = xml.getChildNodes().item(i);
				switch(childNode.getNodeName()) {
				 	
					case REPORT_SAVED_DATE:
						String saveDateStr = childNode.getFirstChild().getNodeValue();
						try {
							Date saveDate = format.parse(saveDateStr);
							setReportSavedDate(saveDate);
						} catch(Exception e) {
							
							log.error("Unable to parse date, skipping it: " + saveDateStr);
						}
						break;
					case PATIENT_NAME:
						Node patientNode = childNode.getFirstChild();
						if(patientNode != null) {
							
							setPatientName(patientNode.getNodeValue());
						}
						break;
					case REPORT_NOTES:
						Node reportNode = childNode.getFirstChild();
						if(reportNode != null) {
							
							setReportNotes(reportNode.getNodeValue());
						}
						break;
					default:
						errors.add("Unable to parse node: " + childNode.getNodeName());
				}
			} catch(Exception e) {
				
				errors.add(xml.getNodeName() + " : " + e.getMessage());
			}
		}
	}
	
	public String toXML() {
		
		StringBuilder result = new StringBuilder();
		
		result.append("<").append(REPORT_NODE).append(">")
			.append("<").append(SETTINGS_NODE).append(">")
			.append(getSettingsXML())
			.append("</").append(SETTINGS_NODE).append(">")
			.append("<").append(SEARCH_INFO_NODE).append(">")
			.append("<" + REPORT_SAVED_DATE + ">" + getReportSavedDate() + "</" + REPORT_SAVED_DATE + ">")
			.append("<" + REPORT_NOTES + ">" + getReportNotes() + "</" + REPORT_NOTES + ">")
			.append("<" + PATIENT_NAME + ">" + getPatientName() + "</" + PATIENT_NAME + ">")
			.append("</").append(SEARCH_INFO_NODE).append(">")
			.append("<").append(FIELDS_NODE).append(">");
		
		for(int i = 0; i < itemIds.size(); i++) {
			
			XMLObject obj = items.get(itemIds.get(i));
			result.append(obj.toXML());
		}
	
		result.append("</").append(FIELDS_NODE).append(">")
			.append("</").append(REPORT_NODE).append(">");
		
		return result.toString();
	}

	@Override
	public void parse(String input) throws ParseException {

		List<String> errors = new ArrayList<>();
		
		try {
			
			InputStream stream = new ByteArrayInputStream(input.getBytes());
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(stream);
			doc.getDocumentElement().normalize();
			parseXML(doc.getFirstChild(), errors, tagsAndIds);
		} catch(Exception e) {
			
			log.error("Error converting string to Document: " + PsychReportUtils.getStackTrace(e));
			throw new ParseException("Unable to parse string", 0);
		}
		
		if(errors.size() > 0) {
			
			log.error("Errors parsing xml: " + PsychReportUtils.convertErrorsListToString(errors));
			throw new ParseException("Unable to parse xml. See Log for details", 0);
		}
	}

	@Override
	public String output() {

		return toXML();
	}
	
	public String getSearchString() {
		
		String result = "Name: " + getPatientName() + "       Date: " + reportSavedDate.toString() + "      Notes: " + getReportNotes();
		
		return result;
	}
	
	public Map<String, List<String>> getTagsAndIds() {
		
		return tagsAndIds;
	}
	
	public Map<String, String> getTagsAndReplacements() {
		
		return tagsAndReplacements;		
	}
	
	public void setTagsAndReplacements(Map<String, String> tagsAndReplacements) {
		
		this.tagsAndReplacements = tagsAndReplacements;
	}
}
