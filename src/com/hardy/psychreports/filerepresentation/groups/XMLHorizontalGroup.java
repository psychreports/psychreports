package com.hardy.psychreports.filerepresentation.groups;

import java.util.List;
import java.util.Map;

import javafx.geometry.Insets;
import javafx.scene.layout.HBox;

import org.w3c.dom.Node;

import com.hardy.psychreports.filerepresentation.XMLObject;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 725 Sandhill Ct
 * Lehi, Ut. 84043
 * 
 * Description: Represents a horizontal group of items for display. The items
 *              are aligned in a row when possible.
 * 
 * Author: Hardy Cherry
 **/
public class XMLHorizontalGroup extends XMLGroup {

	public static final String HORIZONTALGROUP_NODE_NAME = "HorizontalGroup";
	
	public XMLHorizontalGroup() {

		super();
	}
	
	@Override
	public void parseXML(Node xml, List<String> errors, Map<String, List<String>> tagsAndIds) {

		parseXML(xml, errors, HORIZONTALGROUP_NODE_NAME, tagsAndIds);
	}

	@Override
	public String toXML() {

		return toXML(HORIZONTALGROUP_NODE_NAME);
	}
	
	@Override
	public javafx.scene.Node createSeceneNode() {
	
		HBox node = new HBox();
		node.setId(getId());
		node.setManaged(true);
		
		for(int i = 0; i < itemNames.size(); i++) {
			
			XMLObject xmlObj = items.get(itemNames.get(i));
			javafx.scene.Node child = xmlObj.createSeceneNode();
			node.getChildren().add(child);
		}
		
		node.setPadding(new Insets(5,0,5,0));
		
		return node;
	}
	
	@Override
	public String getNodeName() {

		return HORIZONTALGROUP_NODE_NAME;
	}
}
