package com.hardy.psychreports.controllers.controls;

import org.apache.log4j.Logger;

import com.hardy.psychreports.daos.ReportModel;
import com.hardy.psychreports.filerepresentation.XMLObject;
import com.hardy.psychreports.filerepresentation.error.XMLObjectUpdateException;
import com.hardy.psychreports.utils.PsychReportUtils;
import com.hardy.psychreports.views.controls.PRTitledPane;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.scene.control.Accordion;
import javafx.scene.control.TitledPane;
import javafx.scene.input.MouseEvent;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 725 Sandhill Ct
 * Lehi, Ut. 84043
 * 
 * Description:
 * 
 * Author: Hardy Cherry
 **/
public class TitlePaneController {

	private static Logger log = Logger.getLogger(TitlePaneController.class);
	
	public static ChangeListener<TitledPane> changeListener = new ChangeListener<TitledPane>() {
	
		@Override
		public void changed(ObservableValue<? extends TitledPane> observable, final TitledPane oldValue, final TitledPane newValue) {

			if(newValue != null) {
				Accordion a = (Accordion)((PRTitledPane)observable.getValue()).getParentAccordion();
				a.setMaxHeight(Accordion.USE_PREF_SIZE);
				a.setPrefHeight(Accordion.USE_COMPUTED_SIZE);
			}
			if(oldValue != null) {
				Accordion a = (Accordion)((PRTitledPane)oldValue).getParentAccordion();
				a.setMaxHeight(22);
				a.setPrefHeight(22);
			}
		}
	};

	public static EventHandler<MouseEvent> includeBoxClickedEventHandler = new EventHandler<MouseEvent>() {

		@Override
		public void handle(MouseEvent event) {

			Object source = event.getSource();

			if (source instanceof TitledPane) {

				TitledPane titledPane = (TitledPane) source;
				
				String id = titledPane.getId();
				
				XMLObject xmlObj = ReportModel.getInstance().getXMLFile().getItemById(id);
				
				try {
					
					if (titledPane.isExpanded()) {

						titledPane.setText("Included in Report");
						xmlObj.update(new Boolean(true));
					} else {
	
						titledPane.setText("Not included in Report");
						xmlObj.update(new Boolean(false));
					}
				} catch(XMLObjectUpdateException e) {
					
					log.error("Unable to update included in report: " + PsychReportUtils.getStackTrace(e));
				}
			}
		}
	};
}
