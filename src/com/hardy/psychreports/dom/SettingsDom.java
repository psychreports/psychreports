package com.hardy.psychreports.dom;

import java.io.File;
import java.text.ParseException;

import org.apache.log4j.Logger;

import com.hardy.psychreports.daos.FileAccess;
import com.hardy.psychreports.daos.error.FileAccessException;
import com.hardy.psychreports.dom.ParseableDom;
import com.hardy.psychreports.dom.WritableDom;
import com.hardy.psychreports.utils.PsychReportUtils;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 725 Sandhill Ct
 * Lehi, Ut. 84043
 * 
 * Description: A Domain level class that defines the settings for the application
 * 
 * Author: Hardy Cherry
 **/
public class SettingsDom implements WritableDom, ParseableDom {

	private static Logger log = Logger.getLogger(SettingsDom.class);
	
	private String reportDirectory;
	private String templateDirectory;
	private String settingsDirectory;

	/**
	 * Constructor. Also sets up default paths for saved reports and settings.
	 */
	public SettingsDom() {

		File file = null;
		
		try {
		
			String location = FileAccess.getRootPath();
			
			file = new File(location);
			//sets up a default directory for reports
			reportDirectory = file.getPath() + File.separator + "Reports" + File.separator;
			//sets up a default directory for the saved settings.
			settingsDirectory = file.getPath();
			//sets up a default directory for templates
			templateDirectory = file.getPath() + File.separator + "template" + File.separator;
		} catch(FileAccessException e) {
			
			log.error("Unable to initialize the settings class: " + PsychReportUtils.getStackTrace(e));
		}
	}

	/**
	 * @param baseLocation /Reports/ is tacked onto the base location
	 */
	public void setReportDirectory(String baseLocation) {

		reportDirectory = baseLocation + File.separator + "Reports" + File.separator;
	}

	public String getReportDirectory() {

		return reportDirectory;
	}

	public void setSettingsDirectory(String location) {

		settingsDirectory = location;
	}

	public String getSettingsDirectory() {

		return settingsDirectory;
	}
	
	public String getTemplateDirectory() {
		
		return templateDirectory;
	}
	
	/**
	 * @param baseLocation /template/ is tacked onto the base location
	 */
	public void setTemplateDirectory(String baseLocation) {
		
		templateDirectory = baseLocation + File.separator + "template" + File.separator;
	}

	@Override
	public void parse(String input) throws ParseException {

		if (input != null && input.contains("=")) {

			settingsDirectory = input.substring(input.indexOf("=") + 1);
			reportDirectory = settingsDirectory + File.separator + "Reports" + File.separator;
			templateDirectory = settingsDirectory + File.separator + "template" + File.separator;
		} else {

			throw new ParseException("Unable to parse input String: " + input, 0);
		}
	}

	@Override
	public String output() {

		String result = "directory=" + settingsDirectory;
		return result;
	}
}
