package com.hardy.psychreports.controllers;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

import com.hardy.psychreports.daos.FileAccess;
import com.hardy.psychreports.daos.ReportModel;
import com.hardy.psychreports.daos.SettingsModel;
import com.hardy.psychreports.daos.error.ModelException;
import com.hardy.psychreports.filerepresentation.XMLFile;
import com.hardy.psychreports.utils.PsychReportUtils;
import java.text.SimpleDateFormat;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 725 Sandhill Ct
 * Lehi, Ut. 84043
 * 
 * Description:
 * 
 * Author: Hardy Cherry
 **/
public class SearchReportsController implements Initializable {

	@FXML
	protected TextField startDate;
	
	@FXML
	protected TextField endDate;
	
	@FXML
	protected TextField filterTxtInput;
	
	@FXML
	protected ListView<String> listArea;
	
	private static Logger log = Logger.getLogger(SearchReportsController.class);
	private Map<String, XMLFile> allFiles = new HashMap<>();
	private Map<String, XMLFile> filteredFiles = new HashMap<>();
			
	@Override
	public void initialize(URL location, ResourceBundle resources) {

		String reportDirPath = SettingsModel.getInstance().getReportDirectory();
		File reportDir = new File(reportDirPath);
		if(reportDir.exists() && reportDir.isDirectory()) {

			recursivlyFindFiles(reportDir);
			filteredFiles.putAll(allFiles);		
			populateList();
		} else {
			
			ObservableList<String> list = FXCollections.observableArrayList("There are no reports to display, create some reports or go back to the main menu and click the edit menu, select properties, and change the reports path.");
			listArea.setItems(list);
		}
	}
	
	private void recursivlyFindFiles(File file) {
		
		if(file == null) {
			return;
		}
		
		if(file.exists() && file.isDirectory()) {
			
			File[] files = file.listFiles();
			
			for(int i = 0; i < files.length; i++) {
				
				File tempFile = files[i];
				recursivlyFindFiles(tempFile);
			}
		} else if(file.exists() && file.isFile()) {
			
			if(file.getPath().endsWith(".report")) {
				XMLFile xmlFile = new XMLFile();
				String fileContents = FileAccess.readFile(file);
				try {
					
					xmlFile.parse(fileContents);
					allFiles.put(xmlFile.getSearchString(), xmlFile);
				} catch (ParseException e) {
					
					log.error("File: " + file.getAbsolutePath() + " couldn't be opened. " + PsychReportUtils.getStackTrace(e));				
				}
			}
		}
	}
	
	private void populateList() {
		
		if(filteredFiles != null) {
			
			Iterator<String> keyIter = filteredFiles.keySet().iterator();
			while(keyIter.hasNext()) {
				
				String key = keyIter.next();
				listArea.getItems().add(key);
			}
		}
	}

	@FXML
	protected void filterResults() {

		Date startRange = null;
		Date endRange = null;
		String searchString = filterTxtInput.getText();
		
		if(startDate.getText() != null && startDate.getText().length() == 10) {
			
			try {
				
				startRange = SimpleDateFormat.getDateInstance().parse(startDate.getText());
			} catch(Exception e) {
				
			}
		} else {
			
			endDate = null;
			startDate.setText("");
			startDate.setPromptText("Must be in the format mm/dd/yyyy");
		}
		
		if(endDate.getText() != null && endDate.getText().length() == 10) {
			
			try {
				
				endRange = SimpleDateFormat.getDateInstance().parse(endDate.getText());
			} catch(Exception e) {
				
			}
		} else {
			
			endRange = null;
			endDate.setText("");
			endDate.setPromptText("Must be in the format mm/dd/yyyy");
		}
		
		filteredFiles.clear();
		
		Iterator<String> keyIter = allFiles.keySet().iterator();
		while(keyIter.hasNext()) {
			
			String key = keyIter.next();
			XMLFile value = allFiles.get(key);
			
			//If the search string is active then filter by it
			if(searchString != null && startRange == null && endRange == null) {
				
				if(checkString(searchString, key)) {
					
					filteredFiles.put(key, value);
				}
			}			
			//if the dates are active then filter by them
			else if((startRange != null || endRange != null) && searchString == null) {
				
				Date savedDate = value.getReportSavedDate();
				if(checkDate(startRange, endRange, savedDate)) {
					
					filteredFiles.put(key, value);
				}
			}
			//if dates and search string are active filter by both
			else if(searchString != null && (startRange != null || endRange != null)) {
				
				Date savedDate = value.getReportSavedDate();
				if(checkDate(startRange, endRange, savedDate) && checkString(searchString, key)) {
					
					filteredFiles.put(key, value);
				}
			}
			//If no filters are active then everything is added
			else {
				
				filteredFiles.put(key, value);
			}
		}
		
		populateList();
	}

	private boolean checkDate(Date start, Date end, Date toCheck) {
		
		boolean result = false;
		
		if(start != null && start.before(toCheck)) {
				
			result = true;
		}
		
		if(end != null && end.after(toCheck)) {
			
			result = true;
		} else {
			 
			result = false;
		}
		
		return result;
	}
	
	private boolean checkString(String searchString, String toCheck) {
		
		boolean result = false;
		
		if(searchString.contains(toCheck)) {
			
			result = true;
		} else {
			
			result = false;
		}
		
		return result;
	}
	
	@FXML
	protected void back() {

		try {

			ViewChanger.changeView(Views.START_SCREEN_NAME);
		} catch (Exception e) {

			log.error("Unable to go back to start screen: " + PsychReportUtils.getStackTrace(e));
		}
	}

	@FXML
	protected void openSelectedReport() {

		String selectedKey = listArea.getSelectionModel().getSelectedItem();
		try {
			if(filteredFiles.containsKey(selectedKey)) {
			
				XMLFile file = filteredFiles.get(selectedKey);
				ReportModel.getInstance().close();
				ReportModel.getInstance().initializeModel(file);
				
				State.setState(States.EDIT_PRE_LOADED);
				ViewChanger.changeView(Views.NEW_EDIT_SCREEN_NAME);
			}
		} catch (ModelException e) {
			
			log.error("unable to initialize model from search: " + PsychReportUtils.getStackTrace(e));
		} catch (IOException e) {
			
			log.error("unable to change view from search: " + PsychReportUtils.getStackTrace(e));
		}
	}
	
	@FXML
	protected void help() {
		
	}
}
