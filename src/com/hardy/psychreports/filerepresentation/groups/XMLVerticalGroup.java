package com.hardy.psychreports.filerepresentation.groups;

import java.util.List;
import java.util.Map;

import javafx.geometry.Insets;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;

import org.w3c.dom.Node;

import com.hardy.psychreports.filerepresentation.XMLObject;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 725 Sandhill Ct
 * Lehi, Ut. 84043
 * 
 * Description: A vertical group representation. The items are displayed
 *              vertically when possible.
 * 
 * Author: Hardy Cherry
 **/
public class XMLVerticalGroup extends XMLGroup {

	public static final String VERTICALGROUP_NODE_NAME = "VerticalGroup";
	
	public XMLVerticalGroup() {

		super();
	}
	
	@Override
	public void parseXML(Node xml, List<String> errors, Map<String, List<String>> tagsAndIds) {

		parseXML(xml, errors, VERTICALGROUP_NODE_NAME, tagsAndIds);
	}

	@Override
	public String toXML() {

		return toXML(VERTICALGROUP_NODE_NAME);
	}
	
	@Override
	public javafx.scene.Node createSeceneNode() {
	
		AnchorPane anchorpane = new AnchorPane();
		
		VBox node = new VBox();
		node.setManaged(true);
		node.setId(getId());
		
		for(int i = 0; i < itemNames.size(); i++) {
			
			XMLObject xmlObj = items.get(itemNames.get(i));
			javafx.scene.Node child = xmlObj.createSeceneNode();
			node.getChildren().add(child);
		}
		
		anchorpane.getChildren().addAll(node);   // Add grid from Example 1-5
	    AnchorPane.setBottomAnchor(node, 0.0);
	    AnchorPane.setLeftAnchor(node, 0.0);
	    AnchorPane.setRightAnchor(node, 0.0);
	    AnchorPane.setTopAnchor(node, 0.0);
	    
	    anchorpane.setPadding(new Insets(5,0,5,0));
	    
		return anchorpane;
	}
	
	@Override
	public String getNodeName() {

		return VERTICALGROUP_NODE_NAME;
	}
}
