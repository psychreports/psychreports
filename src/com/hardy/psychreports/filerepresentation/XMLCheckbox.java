package com.hardy.psychreports.filerepresentation;

import java.util.List;
import java.util.Map;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.CheckBox;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import com.hardy.psychreports.controllers.ViewChanger;
import com.hardy.psychreports.controllers.controls.CheckBoxController;
import com.hardy.psychreports.filerepresentation.error.XMLObjectUpdateException;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 725 Sandhill Ct
 * Lehi, Ut. 84043
 * 
 * Description: Represents a checkbox for display.
 * 
 * Author: Hardy Cherry
 **/
public class XMLCheckbox extends XMLObject {

	public static final String CHECKBOX_NODE_NAME = "CheckBox";
	public static final String DESCRIPTION_ATTR = "description";
	public static final String DESCRIPTION_WITH_TAGS_ATTR = "descriptionWithTags";
	public static final String CHECKED_ATTR = "checked";
	
	private String description;
	private String descriptionWithTags;
	private boolean checked;

	private final CheckBox node;
	private ChangeListener<Boolean> changeListener = new ChangeListener<Boolean>() {

		@Override
		public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
			
			if(!newValue) {
				
				CheckBoxController.updateModel(node);
			}
		}
	};
	
	public XMLCheckbox() {

		super();
		description = "";
		descriptionWithTags = "";
		checked = false;
		node = new CheckBox();
	}

	public String getDescription() {

		return description;
	}

	public void setDescription(String description) {

		this.description = description;
	}

	public boolean isChecked() {

		return checked;
	}

	public void setChecked(boolean checked) {

		this.checked = checked;
	}

	@Override
	public void parseXML(Node xml, List<String> errors, Map<String, List<String>> tagsAndIds) {

		if (CHECKBOX_NODE_NAME.equals(xml.getNodeName())) {
		
			NamedNodeMap attrs = xml.getAttributes();
			if(attrs != null) {
				
				try {
					setId(attrs.getNamedItem(ID_ATTR).getNodeValue());
					setDescription(attrs.getNamedItem(DESCRIPTION_ATTR).getNodeValue());
					setChecked(Boolean.parseBoolean(attrs.getNamedItem(CHECKED_ATTR).getNodeValue()));
					
					try {
						
						descriptionWithTags = attrs.getNamedItem(DESCRIPTION_WITH_TAGS_ATTR).getNodeValue();
					} catch(Exception e) {
						;//if nothing is here just ignore it.
					}
					
					if(getDescription() != null && getDescription().length() > 0) {
						
						if(descriptionWithTags.contains(TAG_START) && descriptionWithTags.contains(TAG_END)) {
							
							extractTags(getId(), descriptionWithTags, tagsAndIds);
						}
					}
				} catch(Exception e) { 
					
					errors.add(xml.getNodeName() + " : " + e.getMessage());
				}
			}				
		}
	}

	@Override
	public String toXML() {

		StringBuilder result = new StringBuilder();
		result.append("<")
			.append(CHECKBOX_NODE_NAME)
			.append(" ")
			.append(DESCRIPTION_ATTR)
			.append("=\"")
			.append(getDescription())
			.append("\" ")
			.append(DESCRIPTION_WITH_TAGS_ATTR)
			.append("=\"")
			.append(descriptionWithTags)
			.append("\" ")
			.append(ID_ATTR)
			.append("=\"")
			.append(getId())
			.append("\" ")
			.append(CHECKED_ATTR)
			.append("=\"")
			.append(isChecked())
			.append("\"/>");
	
		return result.toString();
	}

	@Override
	public javafx.scene.Node createSeceneNode() {

		node.setId(getId());
		node.setManaged(true);
		node.setSelected(isChecked());
		node.setText(getDescription());
		node.setWrapText(true);
		node.setMaxWidth(ViewChanger.DEFAULT_CONTROL_WIDTH);
		node.focusedProperty().addListener(changeListener);
		
		return node;
	}
	
	@Override
	public void clear() {
		
		node.focusedProperty().removeListener(changeListener);
	}

	@Override
	public String getNodeName() {

		return CHECKBOX_NODE_NAME;
	}
	
	@Override
	public void update(Object newValue) throws XMLObjectUpdateException {
	
		if(newValue instanceof Boolean) {
			
			setChecked((Boolean)newValue);
		} else {
			
			throw new XMLObjectUpdateException("Invalid type to update checkbox with.");
		}
	}

	@Override
	public void replaceTags(Map<String, String> tagsAndReplacement) {

		description = doReplacement(descriptionWithTags, tagsAndReplacement);
	}
}
