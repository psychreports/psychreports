package com.hardy.psychreports.filerepresentation.groups;

import java.util.List;
import java.util.Map;

import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.layout.VBox;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import com.hardy.psychreports.controllers.ViewChanger;
import com.hardy.psychreports.filerepresentation.XMLObject;
import com.hardy.psychreports.filerepresentation.error.XMLObjectCreationException;
import com.hardy.psychreports.filerepresentation.factory.XMLObjectFactory;
import com.hardy.psychreports.filerepresentation.groups.XMLGroup;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 725 Sandhill Ct
 * Lehi, Ut. 84043
 * 
 * Description:
 * 
 * Author: Hardy Cherry
 **/
public class XMLTab extends XMLGroup {

	public static final String TAB_NODE_NAME = "Tab";
	public static final String INCLUDE_IN_REPORT_ATTR = "includeInReport";
	public static final String TITLE_ATTR = "title";

	protected String title;
	
	public XMLTab() {
		
		super();
		title = "";
	}
	
	
	public String getTitle() {
	
		return title;
	}

	
	public void setTitle(String title) {
	
		this.title = title;
	}

	@Override
	public void parseXML(Node xml, List<String> errors, Map<String, List<String>> tagsAndIds) {

		if (TAB_NODE_NAME.equals(xml.getNodeName())) {

			NamedNodeMap attrs = xml.getAttributes();
			if (attrs != null) {

				setId(attrs.getNamedItem(ID_ATTR).getNodeValue());	
				setIncludeInReport(attrs.getNamedItem(INCLUDE_IN_REPORT_ATTR).getNodeValue());
				setTitle(attrs.getNamedItem(TITLE_ATTR).getNodeValue());
			}
			
			for(int i = 0; i < xml.getChildNodes().getLength(); i++) {
				
				Node n = xml.getChildNodes().item(i);

				try {
					XMLObject xmlObj = XMLObjectFactory.getInstance(n.getNodeName());
					if(xmlObj != null) {
						
						xmlObj.parseXML(n, errors, tagsAndIds);
						itemNames.add(xmlObj.getId());
						items.put(xmlObj.getId(), xmlObj);
					}
				} catch(XMLObjectCreationException e) {
					
					errors.add(n.getNodeName() + " : " + e.getMessage());
				}
			}
		}
	}

	@Override
	public String toXML() {

		StringBuilder result = new StringBuilder();
		result.append("<")
			.append(TAB_NODE_NAME)
			.append(" ")
			.append(ID_ATTR)
			.append("=\"")
			.append(getId())
			.append("\" ")
			.append(INCLUDE_IN_REPORT_ATTR)
			.append("=\"")
			.append(getIncludeInReport())
			.append("\" ")
			.append(TITLE_ATTR)
			.append("=\"")
			.append(getTitle())
			.append("\">");
		
		for (int i = 0; i < itemNames.size(); i++) {

			XMLObject obj = items.get(itemNames.get(i));
			result.append(obj.toXML());
		}
		
		result.append("</")
			.append(TAB_NODE_NAME)
			.append(">");

		return result.toString();
	}

	/**
	 * Returns null, use createTab()
	 */
	@Override
	public javafx.scene.Node createSeceneNode() {

		return null;
	}
	
	public Tab createTab() {

		ScrollPane pane = new ScrollPane();
		pane.setManaged(true);
		pane.setMaxSize(ViewChanger.DEFAULT_WIDTH, ViewChanger.DEFAULT_HEIGHT-105);
		
		VBox vBox = new VBox();
		vBox.setManaged(true);
				
		Tab tab = new Tab();
		tab.setId(getId());
		tab.setText(getTitle());
		tab.setClosable(false);
				
		for(int i = 0; i < itemNames.size(); i++) {
			
			XMLObject xmlObj = items.get(itemNames.get(i));
			javafx.scene.Node child = xmlObj.createSeceneNode();
			vBox.getChildren().add(child);
		}
		
		pane.setContent(vBox);
		tab.setContent(pane);
		
		return tab;
	}

	@Override
	public String getNodeName() {

		return TAB_NODE_NAME;
	}
}
