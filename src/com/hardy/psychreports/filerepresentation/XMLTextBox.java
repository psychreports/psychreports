package com.hardy.psychreports.filerepresentation;

import java.util.List;
import java.util.Map;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import com.hardy.psychreports.controllers.ViewChanger;
import com.hardy.psychreports.controllers.controls.TextFieldController;
import com.hardy.psychreports.filerepresentation.error.XMLObjectUpdateException;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 725 Sandhill Ct
 * Lehi, Ut. 84043
 * 
 * Description: Represents a textbox.
 * 
 * Author: Hardy Cherry
 **/
/**
 * 
 * @author Hardy Cherry
 * 
 */
public class XMLTextBox extends XMLObject {

	public static final String TEXT_BOX_NODE_NAME = "TextBox";
	public static final String DESCRIPTION_ATTR = "description";
	public static final String CONTENTS_ATTR = "contents";
	public static final String CONTENTS_WITH_TAGS_ATTR = "contentsWithTags";
	
	private String description;
	private String contents;
	private String contentsWithTags;
	
	private final TextField textField;
	private ChangeListener<Boolean> changeListener = new ChangeListener<Boolean>() {

		@Override
		public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {

			if(!newValue) {
				
				//Update the field when we lose focus
				TextFieldController.updateModel(textField);
			}
		}
	};

	public XMLTextBox() {

		super();
		description = "";
		contents = "";
		contentsWithTags = "";
		textField = new TextField();
	}

	public String getDescription() {

		return description;
	}

	public void setDescription(String description) {

		this.description = description;
	}

	public String getContents() {

		return contents;
	}

	public void setContents(String contents) {

		this.contents = contents;
	}
	
	@Override
	public void parseXML(Node xml, List<String> errors, Map<String, List<String>> tagsAndIds) {

		if (TEXT_BOX_NODE_NAME.equals(xml.getNodeName())) {
		
			NamedNodeMap attrs = xml.getAttributes();
			if(attrs != null) {
				
				try {
					setId(attrs.getNamedItem(ID_ATTR).getNodeValue());
					setDescription(attrs.getNamedItem(DESCRIPTION_ATTR).getNodeValue());
					setContents(attrs.getNamedItem(CONTENTS_ATTR).getNodeValue());
					
					try {
						
						contentsWithTags = attrs.getNamedItem(CONTENTS_WITH_TAGS_ATTR).getNodeValue();
					} catch(Exception e) {
						;//if nothing is here just ignore it.
					}
					
					if(getDescription() != null && getDescription().length() > 0) {
						
						if(contentsWithTags.contains(TAG_START) && contentsWithTags.contains(TAG_END)) {
							
							extractTags(getId(), contentsWithTags, tagsAndIds);
						}
					}
				} catch(Exception e) { 
					
					errors.add(xml.getNodeName() + " : " + e.getMessage());
				}
			}				
		}
	}

	@Override
	public String toXML() {

		StringBuilder result = new StringBuilder();
		result.append("<")
			.append(TEXT_BOX_NODE_NAME)
			.append(" ")
			.append(DESCRIPTION_ATTR)
			.append("=\"")
			.append(getDescription())
			.append("\" ")
			.append(ID_ATTR)
			.append("=\"")
			.append(getId())
			.append("\" ")
			.append(CONTENTS_ATTR)
			.append("=\"")
			.append(getContents())
			.append("\" ")
			.append(CONTENTS_WITH_TAGS_ATTR)
			.append("=\"")
			.append(contentsWithTags)
			.append("\"/>");
	
		return result.toString();
	}

	@Override
	public javafx.scene.Node createSeceneNode() {

		HBox node = new HBox();
		node.setManaged(true);
		
		Label description = new Label();
		description.setManaged(true);
		description.setText(getDescription());
		description.setWrapText(true);
		description.setMaxWidth(ViewChanger.DEFAULT_CONTROL_WIDTH);
		
		textField.setId(getId());
		textField.setManaged(true);
		textField.setText(getContents());
		textField.focusedProperty().addListener(changeListener);
		textField.setMaxWidth(ViewChanger.DEFAULT_CONTROL_WIDTH);
				
		node.getChildren().add(description);
		node.getChildren().add(textField);
		
		return node;
	}
	
	@Override
	public void clear() {
		
		textField.focusedProperty().removeListener(changeListener);
	}

	@Override
	public String getNodeName() {

		return TEXT_BOX_NODE_NAME;
	}
	
	@Override
	public void update(Object newValue) throws XMLObjectUpdateException {
	
		if(newValue instanceof String) {
			
			setContents((String)newValue);
		} else {
			
			throw new XMLObjectUpdateException("Invalid type to update TextBox with.");
		}
	}

	@Override
	public void replaceTags(Map<String, String> tagsAndReplacement) {

		contents = doReplacement(contentsWithTags, tagsAndReplacement);
	}
}
