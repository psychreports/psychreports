package com.hardy.psychreports.dom;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 725 Sandhill Ct
 * Lehi, Ut. 84043
 * 
 * Description: An interface that defines what a class needs to be writable.
 * 
 * Author: Hardy Cherry
 **/
public interface WritableDom {

	/**
	 * @return A string representing the data in the class that needs to be persistent.
	 **/
	public String output();
}
