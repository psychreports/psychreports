package com.hardy.psychreports.daos.error;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 725 Sandhill Ct
 * Lehi, Ut. 84043
 * 
 * Description:
 * 
 * Author: Hardy Cherry
 **/
public class ModelException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8375233423595451811L;

	public ModelException(String message) {
		
		super(message);
	}
}
