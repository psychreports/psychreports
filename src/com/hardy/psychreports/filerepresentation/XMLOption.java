package com.hardy.psychreports.filerepresentation;

import java.util.List;
import java.util.Map;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import com.hardy.psychreports.filerepresentation.error.XMLObjectUpdateException;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 725 Sandhill Ct
 * Lehi, Ut. 84043
 * 
 * Description: Represents drop down option for display
 * 
 * Author: Hardy Cherry
 **/
public class XMLOption extends XMLObject {

	public static final String OPTION_NODE_NAME = "Option";
	public static final String CONTENTS_ATTR = "contents";
	public static final String CONTENTS_WITH_TAGS_ATTR = "contentsWithTags";
	
	private String contents;
	private String contentsWithTags;

	public XMLOption() {

		super();
		contents = "";
		contentsWithTags = "";
	}
	
	public void setContents(String contents) {
		
		this.contents = contents;
	}
	
	public String getContents() {
		
		return contents;
	}
	
	@Override
	public void parseXML(Node xml, List<String> errors, Map<String, List<String>> tagsAndIds) {

		if (OPTION_NODE_NAME.equals(xml.getNodeName())) {
			
			NamedNodeMap attrs = xml.getAttributes();
			if(attrs != null) {
				
				try {
					
					setId(attrs.getNamedItem(ID_ATTR).getNodeValue());
					setContents(attrs.getNamedItem(CONTENTS_ATTR).getNodeValue());	
					
					
					try {
						
						contentsWithTags = attrs.getNamedItem(CONTENTS_WITH_TAGS_ATTR).getNodeValue();
					} catch(Exception e) {
						;//if nothing is here just ignore it.
					}
					
					if(getContents() != null && getContents().length() > 0) {
						
						if(contentsWithTags.contains(TAG_START) && contentsWithTags.contains(TAG_END)) {
							
							extractTags(getId(), contentsWithTags, tagsAndIds);
						}
					}
				} catch(Exception e) { 
					
					errors.add(xml.getNodeName() + " : " + e.getMessage());
				}
			}				
		}
	}

	@Override
	public String toXML() {

		StringBuilder result = new StringBuilder();
		result.append("<")
			.append(OPTION_NODE_NAME)
			.append(" ")
			.append(CONTENTS_ATTR)
			.append("=\"")
			.append(getContents())
			.append("\" ")
			.append(CONTENTS_WITH_TAGS_ATTR)
			.append("=\"")
			.append(contentsWithTags)
			.append("\" ")
			.append(ID_ATTR)
			.append("=\"")
			.append(getId())
			.append("\"/>");
	
		return result.toString();
	}
	
	@Override
	public String toString() {
	
		return contents;
	}

	@Override
	public javafx.scene.Node createSeceneNode() {

		return null;
	}

	@Override
	public void clear() {
		
	}
	
	@Override
	public String getNodeName() {

		return OPTION_NODE_NAME;
	}

	@Override
	public void update(Object newValue) throws XMLObjectUpdateException {
	
		throw new XMLObjectUpdateException("Unable to update XML Object, doesn't support updating.");
	}

	@Override
	public void replaceTags(Map<String, String> tagsAndReplacement) {

		contents = doReplacement(contentsWithTags, tagsAndReplacement);
	}
}
