package com.hardy.psychreports.filerepresentation;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javafx.event.ActionEvent;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import com.hardy.psychreports.controllers.ViewChanger;
import com.hardy.psychreports.controllers.controls.DropDownController;
import com.hardy.psychreports.filerepresentation.error.XMLObjectUpdateException;
import com.sun.javafx.collections.ObservableListWrapper;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 725 Sandhill Ct
 * Lehi, Ut. 84043
 * 
 * Description: Represents a drop down list for display.
 * 
 * Author: Hardy Cherry
 **/
public class XMLDropDown extends XMLObject {

	public static final String DROPDOWN_NODE_NAME = "DropDown";
	public static final String TITLE_ATTR = "title";
	public static final String PROMPT_ATTR = "prompt";
	public static final String SELECTED_ATTR = "selected";
	
	private String title;
	private String prompt;
	private String selectedId;
	private List<XMLOption> options;
	
	private ComboBox<XMLOption> cbox;

	public XMLDropDown() {

		super();
		title = "";
		prompt = "";
		selectedId = "";
		options = new ArrayList<>();
	}

	public XMLOption getItemById(String itemId) {
		
		XMLOption result = null;
		
		for(int i = 0; (i < options.size()) && (result == null); i++) {
			
			if(itemId.equals(options.get(i).getId())) {
				
				result = options.get(i);
			}
		}
		
		return result;
	}
	
	public boolean replaceItem(String id, XMLOption xmlObj) {
		
		boolean replacedItem = false;
		
		for(int i = 0; (i < options.size()) && !replacedItem; i++) {
			
			if(id.equals(options.get(i).getId())) {
				
				options.remove(i);
				options.add(i, xmlObj);
				replacedItem = true;
			}
		}	
		
		return replacedItem;
	}
	
	public String getPrompt() {
	
		return prompt;
	}

	
	public void setPrompt(String prompt) {
	
		this.prompt = prompt;
	}

	
	public String getSelectedId() {
	
		return selectedId;
	}

	
	public void setSelectedId(String selectedId) {
	
		this.selectedId = selectedId;
	}

	
	public List<XMLOption> getOptions() {
	
		return options;
	}

	
	public void setOptions(List<XMLOption> options) {
	
		this.options = options;
	}

	public String getTitle() {

		return title;
	}


	public void setTitle(String title) {

		this.title = title;
	}

	@Override
	public void parseXML(Node xml, List<String> errors, Map<String, List<String>> tagsAndIds) {
		
		if (DROPDOWN_NODE_NAME.equals(xml.getNodeName())) {

			NamedNodeMap attrs = xml.getAttributes();
			if (attrs != null) {

				setId(attrs.getNamedItem(ID_ATTR).getNodeValue());				
				setTitle(attrs.getNamedItem(TITLE_ATTR).getNodeValue());
				setPrompt(attrs.getNamedItem(PROMPT_ATTR).getNodeValue());
				setSelectedId(attrs.getNamedItem(SELECTED_ATTR).getNodeValue());
			}
			
			for(int i = 0; i < xml.getChildNodes().getLength(); i++) {
				
				Node n = xml.getChildNodes().item(i);

				if(XMLOption.OPTION_NODE_NAME.equals(n.getNodeName())) {
					
					XMLOption xmlOpt = new XMLOption();
					xmlOpt.parseXML(n, errors, tagsAndIds);
					options.add(xmlOpt);
				}
			}
		}
	}

	@Override
	public String toXML() {
		
		StringBuilder result = new StringBuilder();
		result.append("<")
			.append(DROPDOWN_NODE_NAME)
			.append(" ")
			.append(ID_ATTR)
			.append("=\"")
			.append(getId())
			.append("\" ")
			.append(TITLE_ATTR)
			.append("=\"")
			.append(getTitle())
			.append("\" ")
			.append(PROMPT_ATTR)
			.append("=\"")
			.append(getPrompt())
			.append("\" ")
			.append(SELECTED_ATTR)
			.append("=\"")
			.append(getSelectedId())
			.append("\">");
		
		for (int i = 0; i < options.size(); i++) {

			XMLOption opt = options.get(i);
			result.append(opt.toXML());
		}
		
		result.append("</")
			.append(DROPDOWN_NODE_NAME)
			.append(">");

		return result.toString();
	}
	
	@Override
	public javafx.scene.Node createSeceneNode() {

		HBox node = new HBox();
		node.setManaged(true);
		
		Label label = new Label(title);
		label.setWrapText(true);
		label.setMaxWidth(ViewChanger.DEFAULT_CONTROL_WIDTH);
		
		cbox = new ComboBox<XMLOption>();
		cbox.setId(getId());
		cbox.setManaged(true);
		cbox.setPromptText(getPrompt());
		cbox.setItems(new ObservableListWrapper<XMLOption>(options));
		cbox.setOnMouseClicked(DropDownController.mouseClickedEventHandler);
		cbox.setOnAction(DropDownController.onActionEventHandler);
		
		int selectedIndex = -1;
		for(int i = 0; (i < options.size()) && (selectedIndex == -1); i++) {
			
			if(options.get(i).getId().equals(selectedId)) {
				
				selectedIndex = i;
			}
		}
		
		if(selectedIndex >= 0) {
			cbox.setValue(options.get(selectedIndex));
		}
				
		node.getChildren().add(label);
		node.getChildren().add(cbox);
		
		return node;
	}
	
	@Override
	public void clear() {
		
		if(options != null) {
			
			for(int i = 0; i < options.size(); i++) {
				
				XMLOption tempOption = options.get(i);
				tempOption.clear();
			}
		}
		
		if(cbox != null) {
			cbox.removeEventHandler(MouseEvent.MOUSE_CLICKED, DropDownController.mouseClickedEventHandler);
			cbox.removeEventHandler(ActionEvent.ACTION, DropDownController.onActionEventHandler);
		}
	}

	@Override
	public String getNodeName() {

		return DROPDOWN_NODE_NAME;
	}
	
	@Override
	public void update(Object newValue) throws XMLObjectUpdateException {
	
		if(newValue instanceof String) {
			
			setSelectedId((String)newValue);
		} else {
			
			throw new XMLObjectUpdateException("Invalid type to update Dropdown with.");
		}
		
	}

	@Override
	public void replaceTags(Map<String, String> tagsAndReplacement) {

		// TODO Auto-generated method stub		
	}
}
