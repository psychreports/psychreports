package com.hardy.psychreports.filerepresentation.constants;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 725 Sandhill Ct
 * Lehi, Ut. 84043
 * 
 * Description: Contains constant values for Fonts.
 * 
 * Author: Hardy Cherry
 **/
public class Font {

	public static final int DEFAULT_SIZE = 12;
	public static final int LARGE_SIZE = 14;
	public static final int TITLE_SIZE = 16;
	public static final int SMALL_SIZE = 10;
}
