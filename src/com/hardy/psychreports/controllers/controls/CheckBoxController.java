package com.hardy.psychreports.controllers.controls;

import org.apache.log4j.Logger;

import com.hardy.psychreports.daos.ReportModel;
import com.hardy.psychreports.filerepresentation.XMLObject;
import com.hardy.psychreports.filerepresentation.error.XMLObjectUpdateException;
import com.hardy.psychreports.utils.PsychReportUtils;

import javafx.scene.control.CheckBox;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 725 Sandhill Ct
 * Lehi, Ut. 84043
 * 
 * Description:
 * 
 * Author: Hardy Cherry
 **/
public class CheckBoxController {

	private static Logger log = Logger.getLogger(CheckBoxController.class);
	
	public static void updateModel(Object source) {
		
		if(source instanceof CheckBox) {
			
			CheckBox checkBox = (CheckBox)source;
			
			String id = checkBox.getId();
			
			XMLObject xmlObj = ReportModel.getInstance().getXMLFile().getItemById(id);
			
			try {
				
				xmlObj.update(checkBox.isSelected());
			} catch(XMLObjectUpdateException e) {
				
				log.error("Unable to update checkbox: " + PsychReportUtils.getStackTrace(e));
			}
		}
	}
}
