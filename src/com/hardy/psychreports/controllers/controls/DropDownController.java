package com.hardy.psychreports.controllers.controls;

import org.apache.log4j.Logger;

import com.hardy.psychreports.daos.ReportModel;
import com.hardy.psychreports.filerepresentation.XMLObject;
import com.hardy.psychreports.filerepresentation.XMLOption;
import com.hardy.psychreports.filerepresentation.error.XMLObjectUpdateException;
import com.hardy.psychreports.utils.PsychReportUtils;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.ComboBox;
import javafx.scene.input.MouseEvent;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 725 Sandhill Ct
 * Lehi, Ut. 84043
 * 
 * Description:
 * 
 * Author: Hardy Cherry
 **/
public class DropDownController {

	private static Logger log = Logger.getLogger(DropDownController.class);
	
	public static EventHandler<ActionEvent> onActionEventHandler = new EventHandler<ActionEvent>() {
		
		@Override
		public void handle(ActionEvent event) {
		
			Object source = event.getSource();
			updateModel(source);	
		}
	};
	
	public static EventHandler<MouseEvent> mouseClickedEventHandler = new EventHandler<MouseEvent>() {

		@Override
		public void handle(MouseEvent event) {

			Object source = event.getSource();
			updateModel(source);	
		}
		
	};
	
	private static void updateModel(Object source) {
		
		if(source instanceof ComboBox) {
			
			ComboBox<XMLOption> textField = (ComboBox<XMLOption>)source;
			
			String id = textField.getId();
			
			XMLObject xmlObj = ReportModel.getInstance().getXMLFile().getItemById(id);
			
			try {
				
				XMLOption option = textField.getValue();
				if(option != null) {
					
					xmlObj.update(option.getId());
				} else {
					
					xmlObj.update("");
				}
			} catch(XMLObjectUpdateException e) {
				
				log.error("Unable to update textfield: " + PsychReportUtils.getStackTrace(e));
			}
		}
	}
}
