package com.hardy.psychreports.utils;

import java.util.Iterator;

import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.layout.Pane;

import com.hardy.psychreports.controllers.ViewChanger;
import com.hardy.psychreports.filerepresentation.XMLFile;
import com.hardy.psychreports.filerepresentation.XMLObject;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 725 Sandhill Ct
 * Lehi, Ut. 84043
 * 
 * Description:
 * 
 * Author: Hardy Cherry
 **/
public class ScenePopulator {

	public static Pane PopulateScene(XMLFile file) {
			
		Pane node = new Pane();
		node.setMinSize(ViewChanger.DEFAULT_WIDTH, ViewChanger.DEFAULT_HEIGHT);
		node.setMaxSize(ViewChanger.DEFAULT_WIDTH, ViewChanger.DEFAULT_HEIGHT);
		node.setPadding(new Insets(10, 10, 10, 10));
		
		Iterator<String> itemIdIter = file.getItemIdIterator();
		while(itemIdIter.hasNext()) {
			
			String itemId = itemIdIter.next();
			XMLObject xmlObj = file.getItemById(itemId);
			Node sceneNode = xmlObj.createSeceneNode();
			node.getChildren().add(sceneNode);
		}
		
		return node;
	}
}
