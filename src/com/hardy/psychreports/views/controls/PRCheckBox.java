package com.hardy.psychreports.views.controls;

import javafx.scene.Node;
import javafx.scene.control.CheckBox;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 725 Sandhill Ct
 * Lehi, Ut. 84043
 * 
 * Description:
 * 
 * Author: Hardy Cherry
 **/
public class PRCheckBox extends CheckBox {

	private Node group;
	
	public PRCheckBox(String label) {
		
		super(label);
	}
	
	public void setGroup(Node group) {
		
		this.group = group;
	}
	
	public Node getGroup() {
		
		return group;
	}
}
