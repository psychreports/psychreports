package com.hardy.psychreports.controllers;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

import com.hardy.psychreports.PsychReports;
import com.hardy.psychreports.daos.ReportModel;
import com.hardy.psychreports.daos.SettingsModel;
import com.hardy.psychreports.daos.error.ModelException;
import com.hardy.psychreports.filerepresentation.XMLFile;
import com.hardy.psychreports.filerepresentation.XMLObject;
import com.hardy.psychreports.utils.PsychReportUtils;
import com.hardy.psychreports.utils.ScenePopulator;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 725 Sandhill Ct
 * Lehi, Ut. 84043
 * 
 * Description:
 * 
 * Author: Hardy Cherry
 **/
public class NewEditReportController implements Initializable {

	private static Logger log = Logger.getLogger(NewEditReportController.class);
			
	private States currentState = States.NONE;
	private XMLFile reportInformation = null;
	private ReportModel reportModel;
	private SettingsModel settingsModel;
	
	@FXML
	protected Pane displayPane;
	
	@FXML
	protected TextField patientNameField;
	
	@FXML
	protected TextField reportNotesField;
	
	@FXML
	protected Label patientNameLbl;
	
	@FXML
	protected Label reportNotesLbl;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {

		reportModel = ReportModel.getInstance();
		settingsModel = SettingsModel.getInstance();
		
		currentState = State.getState();
		
		switch(currentState) {
			
			case EDIT:
				openReportForEdit();
				break;
			case NEW:
				openTemplate();
				break;
			case EDIT_PRE_LOADED:
				editFileFromSearch();
			case EDIT_LOAD_TAGS:
				editLoadTags();
				break;
			default:
				break;		
		}
		
		if(reportInformation != null) {
			
			populateForm(reportInformation);
		}
	}
	
	private void openReportForEdit() {
		
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Open a saved report");
		fileChooser.setInitialDirectory(new File(settingsModel.getReportDirectory()));
		 //Set extension filter
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("Report files (*" + XMLFile.REPORT_EXT + ")", "*" + XMLFile.REPORT_EXT + "");
        fileChooser.getExtensionFilters().add(extFilter);
        
		File report = fileChooser.showOpenDialog(PsychReports.getPrimaryStage());
		
		if(report != null) {
			
			try {
				
				reportModel.initializeModel(report);
				reportModel.setSavePath(report);
				reportInformation = reportModel.getXMLFile();
			} catch (ModelException e) {
				
				log.error("Error while initializing reportModel: " + PsychReportUtils.getStackTrace(e));
				PsychReportUtils.displayError(e.getMessage());
			}
		}
	}
	
	private void editFileFromSearch() {
		
		reportInformation = reportModel.getXMLFile();
	}
	
	private void editLoadTags() {
		
		reportInformation = reportModel.getXMLFile();

		Map<String, List<String>> tagsAndIds = reportInformation.getTagsAndIds();
		Map<String, String> tagsAndReplacement = reportInformation.getTagsAndReplacements();
		
		Iterator<String> tagIter = tagsAndIds.keySet().iterator();
		while(tagIter.hasNext()) {
			
			String tag = tagIter.next();
			if(tag != null) {
				
				List<String> idsList = tagsAndIds.get(tag);
				
				if(idsList != null) {
					
					for(int i = 0; i < idsList.size(); i++) {
						
						XMLObject xmlObj = reportInformation.getItemById(idsList.get(i));
						if(xmlObj != null) {
							
							xmlObj.replaceTags(tagsAndReplacement);
						}
					}
				}
			}
		}
	}
	
	private void openTemplate() {
		
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Open a template to start a new report");
		fileChooser.setInitialDirectory(new File(settingsModel.getTemplateDirectory()));
		//Set extension filter
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("Template files (*" + XMLFile.TEMPLATE_EXT + ")", "*" + XMLFile.TEMPLATE_EXT + "");
        fileChooser.getExtensionFilters().add(extFilter);
        
		File report = fileChooser.showOpenDialog(PsychReports.getPrimaryStage());
		
		if(report != null) {
			
			try {
				
				reportModel.initializeModel(report);
				reportInformation = reportModel.getXMLFile();
			} catch (ModelException e) {
				
				log.error("Error while initializing reportModel: " + PsychReportUtils.getStackTrace(e));
				PsychReportUtils.displayError(e.getMessage());
			}
		} else {
			
			close();
		}
	}
	
	private void populateForm(XMLFile report) {
		
		Pane pane = ScenePopulator.PopulateScene(report);
		displayPane.getChildren().clear();
		displayPane.getChildren().add(pane);
		displayPane.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		reportNotesField.setText(report.getReportNotes());
		patientNameField.setText(report.getPatientName());		
	}

	@FXML
	protected void save() {
		
		String currSavePath = "";
		boolean readyForSave = false;
		
		if(reportModel.getSaveFile() != null) {
			
			currSavePath = reportModel.getSaveFile().getName();
			if(currSavePath.endsWith(XMLFile.REPORT_EXT)) {
				
				readyForSave = true;
			}
		}
		
		if(!readyForSave) {
			FileChooser fileChooser = new FileChooser();
			fileChooser.setTitle("Type in a name to save the report");
			fileChooser.setInitialDirectory(new File(settingsModel.getReportDirectory()));
			//Set extension filter
	        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("Report files (*" + XMLFile.REPORT_EXT + ")", "*" + XMLFile.REPORT_EXT + "");
	        fileChooser.getExtensionFilters().add(extFilter);
	        
			File report = fileChooser.showSaveDialog(PsychReports.getPrimaryStage());
			
			if(report != null) {
			
				String fileName = report.getName();
				if(fileName.indexOf(".") == -1) {
					
					report = new File(report.getPath() + XMLFile.REPORT_EXT);
				}
				
				reportModel.setSavePath(report);
				readyForSave = true;
			}
		} 
		

		if(patientNameField.getText().trim().length() == 0) {
			
			patientNameLbl.setTextFill(Color.RED);
			readyForSave = false;
		} else {
			
			reportModel.getXMLFile().setPatientName(patientNameField.getText());
		}
		
		if(reportNotesField.getText().trim().length() == 0) {
			
			reportNotesLbl.setTextFill(Color.RED);
			readyForSave = false;
		} else {
			
			reportModel.getXMLFile().setReportNotes(reportNotesField.getText());
		}
		
		if(readyForSave) {
			
			try {
				
				reportModel.save();
			} catch (ModelException e) {
				
				log.error("Error while saving reportModel: " + PsychReportUtils.getStackTrace(e));
				PsychReportUtils.displayError(e.getMessage());
			}
		}
	}
	
	@FXML
	protected void close() {
		
		try {
			if(reportModel.isDirty()) {
				
				save();
			}
			
			reportModel.close();
			ViewChanger.changeView(Views.START_SCREEN_NAME);
		} catch(Exception e) {
			
			log.error("Unable to go back to start screen: " + PsychReportUtils.getStackTrace(e));
		}
	}
	
	@FXML
	protected void setupTagReplacement() {
		
		try {
			
			ViewChanger.changeView(Views.EDIT_TAGS_NAME);
		} catch (IOException e) {

			log.error("Unable to go to edit tags screen: " + PsychReportUtils.getStackTrace(e));
		}
	}
	
	@FXML
	protected void saveAsPDF() {
		
	}
	
	@FXML
	protected void saveAsDoc() {
		
	}	
	
	@FXML
	protected void help() {
		
	}
}
