package com.hardy.psychreports.filerepresentation;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import com.hardy.psychreports.controllers.ViewChanger;
import com.hardy.psychreports.controllers.controls.TableController;
import com.hardy.psychreports.filerepresentation.constants.Font;
import com.hardy.psychreports.filerepresentation.error.XMLObjectUpdateException;
import com.hardy.psychreports.views.controls.PRButton;
import com.hardy.psychreports.views.controls.PRGridPane;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 725 Sandhill Ct
 * Lehi, Ut. 84043
 * 
 * Description: Represents a table for display.
 * 
 * Author: Hardy Cherry
 **/
public class XMLTable extends XMLObject {

	public static final String TABLE_NODE_NAME = "Table";
	public static final String TITLE_ATTR = "title";
	public static final String DESCRIPTION_ATTR = "description";
	public static final String INPUT_TYPE_ATTR = "inputType";
	public static final String COLUMN_COUNT_ATTR = "columnCount";
	public static final int DEFAULT_TABLE_COLUMN_COUNT = 2;
	public static final int MAX_TABLE_COLUMN_COUNT = 20;
	
	private String title;
	private String description;
	private String inputType;
	private int tableColumnCount;
	private List<XMLTableRow> tableRows;
	
	private PRButton addRowButton;

	public XMLTable() {

		super();
		title = "";
		description = "";
		inputType = null;
		tableRows = new ArrayList<>();
		tableColumnCount = DEFAULT_TABLE_COLUMN_COUNT;
	}

	public XMLObject getItemById(String itemId) {
		
		XMLObject result = null;
		
		for(int i = 0; (i < tableRows.size()) && (result == null); i++) {
			
			if(itemId.equals(tableRows.get(i).getId())) {
				
				result = tableRows.get(i);
			} else {
				
				result = tableRows.get(i).getItemById(itemId);
			}
		}
		
		return result;
	}
	
	public boolean replaceItem(String id, XMLTableRow xmlObj) {
		
		boolean replacedItem = false;
		
		for(int i = 0; (i < tableRows.size()) && !replacedItem; i++) {
			
			if(id.equals(tableRows.get(i).getId())) {
				
				tableRows.remove(i);
				tableRows.add(i, xmlObj);
				replacedItem = true;
			}
		}	
		
		return replacedItem;
	}

	public String getTitle() {

		return title;
	}

	public void setTitle(String title) {

		this.title = title;
	}

	public String getDescription() {

		return description;
	}

	public void setDescription(String description) {

		this.description = description;
	}

	public List<XMLTableRow> getTableRows() {

		return tableRows;
	}

	public void setTableRows(List<XMLTableRow> tableRows) {

		this.tableRows = tableRows;
	}
	
	public String getInputType() {
		
		return inputType;
	}
	
	public void setInputType(String inputType) {
		
		this.inputType = inputType;
	}
	
	public int getColumnCount() {
		
		if(tableColumnCount < 1) {
			
			tableColumnCount = DEFAULT_TABLE_COLUMN_COUNT;
		} else if(tableColumnCount > MAX_TABLE_COLUMN_COUNT) {
			
			tableColumnCount = MAX_TABLE_COLUMN_COUNT;
		}		
		
		return tableColumnCount;
	}
	
	public void setColumnCount(int columnCount) {
		
		this.tableColumnCount = columnCount;
	}
		
	@Override
	public void parseXML(Node xml, List<String> errors, Map<String, List<String>> tagsAndIds) {

		if (TABLE_NODE_NAME.equals(xml.getNodeName())) {
		
			NamedNodeMap attrs = xml.getAttributes();
			if(attrs != null) {
				
				try {
					setId(attrs.getNamedItem(ID_ATTR).getNodeValue());
					setTitle(attrs.getNamedItem(TITLE_ATTR).getNodeValue());
					setDescription(attrs.getNamedItem(DESCRIPTION_ATTR).getNodeValue());
					setInputType(attrs.getNamedItem(INPUT_TYPE_ATTR).getNodeValue());
					int colCount = DEFAULT_TABLE_COLUMN_COUNT;
					try {
						String colCountStr = attrs.getNamedItem(COLUMN_COUNT_ATTR).getNodeValue();
						colCount = Integer.parseInt(colCountStr);
					} catch(NumberFormatException | NullPointerException e) {
						;//do nothing, just use default.
					}
					setColumnCount(colCount);
				} catch(Exception e) { 
					
					errors.add(xml.getNodeName() + " : " + e.getMessage());
				}
			}	
			
			for(int i = 0; i < xml.getChildNodes().getLength(); i++) {
				
				Node n = xml.getChildNodes().item(i);

				if(XMLTableRow.TABLE_ROW_NODE_NAME.equals(n.getNodeName())) {
					
					XMLTableRow row = new XMLTableRow();
					row.parseXML(n, errors, tagsAndIds);
					tableRows.add(row);
				}
			}
		}
	}

	@Override
	public String toXML() {

		StringBuilder result = new StringBuilder();
		result.append("<")
			.append(TABLE_NODE_NAME)
			.append(" ")
			.append(TITLE_ATTR)
			.append("=\"")
			.append(getTitle())
			.append("\" ")
			.append(ID_ATTR)
			.append("=\"")
			.append(getId())
			.append("\" ")
			.append(DESCRIPTION_ATTR)
			.append("=\"")
			.append(getDescription())
			.append("\" ")
			.append(INPUT_TYPE_ATTR)
			.append("=\"")
			.append(getInputType())
			.append("\" ")
			.append(COLUMN_COUNT_ATTR)
			.append("=\"")
			.append(getColumnCount())
			.append("\">");
		
		for(int i = 0; i < tableRows.size(); i++) {
			
			XMLTableRow row = tableRows.get(i);
			result.append(row.toXML());
		}
		
		result.append("</")
			.append(TABLE_NODE_NAME)
			.append(">");
	
		return result.toString();
	}

	@Override
	public javafx.scene.Node createSeceneNode() {

		VBox node = new VBox();
		node.setId(getId());
		node.setManaged(true);
		
		PRGridPane gridPane = new PRGridPane();
		
		gridPane.setManaged(true);
		gridPane.setPadding(new Insets(2.5, 0.0, 5.0, 0.0));
		
		Label label = new Label();
		label.setManaged(true);
		label.setText(getTitle());
		label.setWrapText(true);
		label.setMaxWidth(200);
		label.setFont(new javafx.scene.text.Font(Font.LARGE_SIZE));
		
		Label dash = new Label(" - ");
		label.setManaged(true);
		label.setFont(new javafx.scene.text.Font(Font.DEFAULT_SIZE));
				
		Label description = new Label();
		description.setManaged(true);
		description.setText(getDescription());
		description.setWrapText(true);
		description.setMaxWidth(ViewChanger.DEFAULT_CONTROL_WIDTH-250);
		description.setFont(new javafx.scene.text.Font(Font.DEFAULT_SIZE));

		Label columnCountLbl = new Label();
		columnCountLbl.setManaged(true);
		columnCountLbl.setText("       Print columns:");
		columnCountLbl.setWrapText(true);
		columnCountLbl.setMaxWidth(ViewChanger.DEFAULT_CONTROL_WIDTH-250);
		columnCountLbl.setFont(new javafx.scene.text.Font(Font.DEFAULT_SIZE));
		
		TextField columnCountTxt = new TextField();
		columnCountTxt.setManaged(true);
		columnCountTxt.setText(""+getColumnCount());
		columnCountTxt.setMaxWidth(25);
		
		HBox title = new HBox();
		title.setManaged(true);
		title.getChildren().add(label);
		if(getTitle() != null && getTitle().length() > 0 && 
				getDescription() != null && getDescription().length() > 0) {
		
			title.getChildren().add(dash);
		}
		title.getChildren().add(description);
		title.getChildren().add(columnCountLbl);
		title.getChildren().add(columnCountTxt);
		
		gridPane.setHgap(10.0);
		gridPane.setVgap(5.0);
		
		for(int i = 0; i < tableRows.size(); i++) {
			
			gridPane.incrementRow();
			
			if(tableRows.get(i).getTableData() != null) {
				
				for(int j = 0; j < tableRows.get(i).getTableData().size(); j++) {
								
					XMLTableData td = tableRows.get(i).getTableData().get(j);
					javafx.scene.Node child = td.createSeceneNode();
				
					if(child != null) {
						gridPane.add(child);
					}

					gridPane.incrementColumn();
				}
				
				gridPane.resetColumn();
			}
		}
		
		addRowButton = new PRButton();
		addRowButton.setText("+ Add Row");
		addRowButton.setId(getId());
		addRowButton.setTableDataContentType(inputType);
		addRowButton.setTableNode(gridPane);
		addRowButton.setOnMouseClicked(TableController.addRowEventHandler);
		
		node.getChildren().add(title);
		node.getChildren().add(gridPane);
		if(inputType != null && inputType.length() > 0) {
			
			node.getChildren().add(addRowButton);
		}

		node.setPadding(new Insets(5.0, 0.0, 5.0, 0.0));
		
		return node;
	}
	
	@Override
	public void clear() {
		
		if(tableRows != null) {
			
			for(int i = 0; i < tableRows.size(); i++) {
				
				XMLTableRow tempRow = tableRows.get(i);
				tempRow.clear();
			}
		}
		
		if(addRowButton != null) {
			
			addRowButton.removeEventHandler(MouseEvent.MOUSE_CLICKED, TableController.addRowEventHandler);
		}
	}

	@Override
	public String getNodeName() {

		return TABLE_NODE_NAME;
	}
	
	@Override
	public void update(Object newValue) throws XMLObjectUpdateException {
	
		if(newValue instanceof XMLTableRow) {
			
			tableRows.add((XMLTableRow)newValue);
		} else {
			
			throw new XMLObjectUpdateException("Invalid type to update Table with.");
		}
	}

	@Override
	public void replaceTags(Map<String, String> tagsAndReplacement) {

		// TODO Auto-generated method stub		
	}
}
