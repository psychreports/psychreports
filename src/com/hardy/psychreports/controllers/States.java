package com.hardy.psychreports.controllers;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 725 Sandhill Ct
 * Lehi, Ut. 84043
 * 
 * Description: The States the application can be in. 
 * None
 * New - creating a new report
 * Edit - editing an existing report
 * 
 * Author: Hardy Cherry
 **/
public enum States {

	NONE,
	NEW, 
	EDIT,
	EDIT_PRE_LOADED,
	EDIT_LOAD_TAGS
}
