package com.hardy.psychreports.controllers.controls;

import java.util.Random;

import org.apache.log4j.Logger;

import com.hardy.psychreports.daos.ReportModel;
import com.hardy.psychreports.filerepresentation.XMLObject;
import com.hardy.psychreports.filerepresentation.XMLTable;
import com.hardy.psychreports.filerepresentation.XMLTableData;
import com.hardy.psychreports.filerepresentation.XMLTableRow;
import com.hardy.psychreports.filerepresentation.error.XMLObjectCreationException;
import com.hardy.psychreports.utils.PsychReportUtils;
import com.hardy.psychreports.views.controls.PRButton;
import com.hardy.psychreports.views.controls.PRGridPane;

import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 725 Sandhill Ct
 * Lehi, Ut. 84043
 * 
 * Description:
 * 
 * Author: Hardy Cherry
 **/
public class TableController {

	private static Logger log = Logger.getLogger(TextFieldController.class);
	
	public static EventHandler<MouseEvent> addRowEventHandler = new EventHandler<MouseEvent>() {
		
		@Override
		public void handle(MouseEvent event) {
		
			Object source = event.getSource();
			
			if(source instanceof PRButton) {
				
				PRButton button = (PRButton)source;
				PRGridPane gridPane = button.getTableNode();
				XMLObject contentType = null;
				
				gridPane.incrementRow();
				
				String id = button.getId();
				
				XMLTable xmlTable = (XMLTable)ReportModel.getInstance().getXMLFile().getItemById(id);
				XMLTableRow newRow = new XMLTableRow();

				Random randGen = new Random();
				long rand = randGen.nextLong();
				
				XMLObject xmlObj = ReportModel.getInstance().getXMLFile().getItemById("newTableRow"+rand);
				while(xmlObj != null) {
					
					rand++;
					xmlObj = ReportModel.getInstance().getXMLFile().getItemById("newTableRow"+rand);
				}
				
				//the 0 element should have the table header information.
				int columns = xmlTable.getTableRows().get(0).getTableData().size();
				
				newRow.setId("newTableRow" + (rand++));
				xmlTable.getTableRows().add(newRow);				

				String newId = "newField";
				
				for(int i = 0; i < columns; i++) {
				
					xmlObj = ReportModel.getInstance().getXMLFile().getItemById(newId+rand);
					while(xmlObj != null) {
						
						rand++;
						xmlObj = ReportModel.getInstance().getXMLFile().getItemById(newId+rand);
					}
					
					try {
						
						contentType = button.getNewTableDataContentType();
						contentType.setId(newId + (rand++));
						
						Node node = contentType.createSeceneNode();
						
						XMLTableData newData = new XMLTableData();
						newData.setData(contentType);
						
						xmlObj = ReportModel.getInstance().getXMLFile().getItemById("newTableData"+rand);
						while(xmlObj != null) {
							
							rand++;
							xmlObj = ReportModel.getInstance().getXMLFile().getItemById("newTableData"+rand);
						}
						newData.setId("newTableData"+ (rand++));
						newRow.getTableData().add(newData);
						
						gridPane.add(node, i);
					} catch(XMLObjectCreationException e) {
						
						log.error("Unable to add content to row: " + PsychReportUtils.getStackTrace(e));
					}
				}

				ReportModel.getInstance().getXMLFile().replaceItem(xmlTable.getId(), xmlTable);
			}			
		}
	};
}
