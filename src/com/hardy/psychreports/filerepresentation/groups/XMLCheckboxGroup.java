package com.hardy.psychreports.filerepresentation.groups;

import java.util.List;
import java.util.Map;

import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import com.hardy.psychreports.filerepresentation.XMLObject;
import com.hardy.psychreports.filerepresentation.error.XMLObjectCreationException;
import com.hardy.psychreports.filerepresentation.factory.XMLObjectFactory;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 725 Sandhill Ct
 * Lehi, Ut. 84043
 * 
 * Description: Represents a group of checkboxes for display.
 * 
 * Author: Hardy Cherry
 **/
public class XMLCheckboxGroup extends XMLGroup {

	public static final String CHECKBOXGROUP_NODE_NAME = "CheckBoxGroup";
	public static final String DESCRIPTION_ATTR = "description";
	public static final String DESCRIPTION_WITH_TAGS_ATTR = "descriptionWithTags";
	public static final String FILL_HORIZONTAL_ATTR = "fillHorizontal";
	
	private String description;
	private String descriptionWithTags;
	private boolean fillHorizontal;

	public XMLCheckboxGroup() {

		super();
		description = "";
		descriptionWithTags = "";
		fillHorizontal = false;
	}

	public String getDescription() {

		return description;
	}

	public void setDescription(String description) {

		this.description = description;
	}

	public boolean isFillHorizontal() {

		return fillHorizontal;
	}

	public void setFillHorizontal(boolean fillHorizontal) {

		this.fillHorizontal = fillHorizontal;
	}

	@Override
	public void parseXML(Node xml, List<String> errors, Map<String, List<String>> tagsAndIds) {

		if (CHECKBOXGROUP_NODE_NAME.equals(xml.getNodeName())) {

			NamedNodeMap attrs = xml.getAttributes();
			if (attrs != null) {

				try {
					
					setId(attrs.getNamedItem(ID_ATTR).getNodeValue());	
					setDescription(attrs.getNamedItem(DESCRIPTION_ATTR).getNodeValue());
					setFillHorizontal(Boolean.parseBoolean(attrs.getNamedItem(FILL_HORIZONTAL_ATTR).getNodeValue()));
					
					try {
						
						descriptionWithTags = attrs.getNamedItem(DESCRIPTION_WITH_TAGS_ATTR).getNodeValue();
					} catch(Exception e) {
						;//if nothing is here just ignore it.
					}
					
					if(getDescription() != null && getDescription().length() > 0) {
						
						if(descriptionWithTags.contains(TAG_START) && descriptionWithTags.contains(TAG_END)) {
							
							extractTags(getId(), descriptionWithTags, tagsAndIds);
						}
					}
				} catch(Exception e) {
					
					errors.add(xml.getNodeName() + " : " + e.getMessage());
				}
			}
			
			for(int i = 0; i < xml.getChildNodes().getLength(); i++) {
				
				Node n = xml.getChildNodes().item(i);

				try {
					XMLObject xmlObj = XMLObjectFactory.getInstance(n.getNodeName());
					if(xmlObj != null) {
						
						xmlObj.parseXML(n, errors, tagsAndIds);
						itemNames.add(xmlObj.getId());
						items.put(xmlObj.getId(), xmlObj);
					}
				} catch(XMLObjectCreationException e) {
					
					errors.add(n.getNodeName() + " : " + e.getMessage());
				}
			}
		}
	}

	@Override
	public String toXML() {

		StringBuilder result = new StringBuilder();
		result.append("<")
			.append(CHECKBOXGROUP_NODE_NAME)
			.append(" ")
			.append(ID_ATTR)
			.append("=\"")
			.append(getId())
			.append("\" ")
			.append(DESCRIPTION_ATTR)
			.append("=\"")
			.append(getDescription())
			.append("\" ")
			.append(DESCRIPTION_WITH_TAGS_ATTR)
			.append("=\"")
			.append(descriptionWithTags)
			.append("\" ")
			.append(FILL_HORIZONTAL_ATTR)
			.append("=\"")
			.append(isFillHorizontal())
			.append("\" ")
			.append(INCLUDE_IN_REPORT_ATTR)
			.append("=\"")
			.append(getIncludeInReport())
			.append("\">");
		
		for (int i = 0; i < itemNames.size(); i++) {

			XMLObject obj = items.get(itemNames.get(i));
			result.append(obj.toXML());
		}
		
		result.append("</")
			.append(CHECKBOXGROUP_NODE_NAME)
			.append(">");

		return result.toString();
	}
	
	@Override
	public javafx.scene.Node createSeceneNode() {
	
		AnchorPane anchorpane = new AnchorPane();
		
		VBox node = new VBox();
		node.setManaged(true);
		node.setId(getId());
		
		Label description = new Label(getDescription());
		node.getChildren().add(description);
		
		for(int i = 0; i < itemNames.size(); i++) {
			
			XMLObject xmlObj = items.get(itemNames.get(i));
			javafx.scene.Node child = xmlObj.createSeceneNode();
			node.getChildren().add(child);
		}
		
		anchorpane.getChildren().addAll(node);   // Add grid from Example 1-5
	    AnchorPane.setBottomAnchor(node, 0.0);
	    AnchorPane.setLeftAnchor(node, 0.0);
	    AnchorPane.setRightAnchor(node, 0.0);
	    AnchorPane.setTopAnchor(node, 0.0);
	    
	    anchorpane.setPadding(new Insets(5,0,5,0));
	    
		return anchorpane;
	}
	
	@Override
	public String getNodeName() {

		return CHECKBOXGROUP_NODE_NAME;
	}
	
	@Override
	public void replaceTags(Map<String, String> tagsAndReplacement) {

		description = doReplacement(descriptionWithTags, tagsAndReplacement);
	}
}
