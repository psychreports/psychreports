package com.hardy.psychreports;

import java.io.File;

import org.apache.log4j.PropertyConfigurator;

import com.hardy.psychreports.controllers.ViewChanger;
import com.hardy.psychreports.controllers.Views;
import com.hardy.psychreports.daos.error.FileAccessException;

import javafx.application.Application;
import javafx.scene.image.Image;
import javafx.stage.Stage;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 725 Sandhill Ct
 * Lehi, Ut. 84043
 * 
 * Description:
 * 
 * Author: Hardy Cherry
 **/
public class PsychReports extends Application {

	private static Stage primaryStage;

	@Override
	public void start(Stage primaryStage) throws Exception {

		primaryStage.getIcons().add(new Image(PsychReports.class.getResourceAsStream("icons/32x32icon.png")));
		
		PsychReports.primaryStage = primaryStage;
		ViewChanger.changeView(Views.START_SCREEN_NAME, -1.0, -1.0);
	}

	public static Stage getPrimaryStage() {

		return primaryStage;
	}
	
	/**
	 * Cleans up and closes any thing that's open giving the user a chance to
	 * save their work.
	 * 
	 * @return true if everything cleaned up without the need for user input.
	 */
	public static boolean cleanUpApplication() {

		boolean result = true;

		// shut down any threads, close/save thier work
		// clean up open files

		return result;
	}


	/**
	 * The main() method is ignored in correctly deployed JavaFX application.
	 * main() serves only as fallback in case the application can not be
	 * launched through deployment artifacts, e.g., in IDEs with limited FX
	 * support. NetBeans ignores main().
	 * 
	 * @param args
	 *            the command line arguments
	 * @throws FileAccessException 
	 */
	public static void main(String[] args) throws FileAccessException {

		//Set up logging folder
		String logDirStr = "log";
		File logDir = new File(logDirStr);		
		logDir.mkdirs();
		
		String iniFilePath = "psychreports.ini";
		
		// Set up a simple configuration that logs on the console.
	    PropertyConfigurator.configure(iniFilePath);
		
		launch(args);
	}
}
