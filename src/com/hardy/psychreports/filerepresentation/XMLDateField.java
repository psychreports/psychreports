package com.hardy.psychreports.filerepresentation;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import com.hardy.psychreports.controllers.controls.DateFieldController;
import com.hardy.psychreports.filerepresentation.constants.Font;
import com.hardy.psychreports.filerepresentation.error.XMLObjectUpdateException;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 725 Sandhill Ct
 * Lehi, Ut. 84043
 * 
 * Description: Represents a Date field for display.
 * 
 * Author: Hardy Cherry
 **/
public class XMLDateField extends XMLObject {

	public static final String DATEFIELD_NODE_NAME = "DateField";
	public static final String DESCRIPTION_ATTR = "description";
	public static final String CONTENTS_ATTR = "contents";

	private String description;
	private Date contents;

	private final TextField textField;
	private ChangeListener<Boolean> changeListener = new ChangeListener<Boolean>() {

		@Override
		public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {

			if(!newValue) {
				
				//Update the field when we lose focus.
				DateFieldController.updateDateField(textField);
			}
		}
	};
	
	public XMLDateField() {

		super();
		description = "";
		contents = null;
		textField = new TextField();
	}

	public String getDescription() {

		return description;
	}

	public void setDescription(String description) {

		this.description = description;
	}

	public Date getContents() {

		return contents;
	}

	public void setContents(Date contents) {

		this.contents = contents;
	}

	public void setContents(String date) {

		SimpleDateFormat f = new SimpleDateFormat("EEE MMM d hh:mm:ss z yyyy");
		try {
			if(date != null && !"".equals(date) && !"null".equalsIgnoreCase(date)) {
				setContents(f.parse(date));
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void parseXML(Node xml, List<String> errors, Map<String, List<String>> tagsAndIds) {

		if (DATEFIELD_NODE_NAME.equals(xml.getNodeName())) {

			NamedNodeMap attrs = xml.getAttributes();
			if (attrs != null) {

				try {
					setId(attrs.getNamedItem(ID_ATTR).getNodeValue());
					setDescription(attrs.getNamedItem(DESCRIPTION_ATTR).getNodeValue());
					setContents(attrs.getNamedItem(CONTENTS_ATTR).getNodeValue());
				} catch (Exception e) {

					errors.add(xml.getNodeName() + " : " + e.getMessage());
				}
			}
		}
	}

	@Override
	public String toXML() {

		StringBuilder result = new StringBuilder();
		result.append("<")
			.append(DATEFIELD_NODE_NAME)
			.append(" ")
			.append(DESCRIPTION_ATTR)
			.append("=\"")
			.append(getDescription())
			.append("\" ")
			.append(ID_ATTR)
			.append("=\"")
			.append(getId())
			.append("\" ")
			.append(CONTENTS_ATTR)
			.append("=\"");
			if(getContents() != null) {
			
				result.append(getContents().toString());
			} else {
				result.append(getContents());
			}
			result.append("\"/>");

		return result.toString();
	}

	@Override
	public javafx.scene.Node createSeceneNode() {

		String simpleDate = "";
		try {
			
			SimpleDateFormat f = new SimpleDateFormat("MM/dd/yyyy");
			simpleDate = f.format(getContents());
		} catch(Exception e) {
			
			simpleDate = "";
		}
		
		HBox node = new HBox();
		node.setManaged(true);
		
		Label description = new Label(getDescription());
		description.setFont(new javafx.scene.text.Font(Font.DEFAULT_SIZE));
		
		textField.setId(getId());
		textField.setText(simpleDate);
		textField.setManaged(true);
		textField.focusedProperty().addListener(changeListener);
		
		node.getChildren().add(description);
		node.getChildren().add(textField);
				
		return node;
	}
	
	@Override
	public void clear() {
		
		textField.focusedProperty().removeListener(changeListener);
	}

	@Override
	public String getNodeName() {

		return DATEFIELD_NODE_NAME;
	}

	@Override
	public void update(Object newValue) throws XMLObjectUpdateException {

		if(newValue instanceof Date) {
			
			setContents((Date)newValue);
		} else if(newValue instanceof String) {
			
			setContents((String)newValue);
		} else if(newValue == null) {
			
			Date d = null;
			setContents(d);
		} else {
			
			throw new XMLObjectUpdateException("Invalid type to update Datefield with.");
		}
		
	}

	@Override
	public void replaceTags(Map<String, String> tagsAndReplacement) {

		// TODO Auto-generated method stub		
	}
}
