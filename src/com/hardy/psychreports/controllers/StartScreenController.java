package com.hardy.psychreports.controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

import com.hardy.psychreports.PsychReports;
import com.hardy.psychreports.utils.PsychReportUtils;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 725 Sandhill Ct
 * Lehi, Ut. 84043
 * 
 * Description:
 * 
 * Author: Hardy Cherry
 **/
public class StartScreenController implements Initializable {

	private static Logger log = Logger.getLogger(StartScreenController.class);
	
	@FXML
	private Button startNewReportBtn;
	@FXML
	private Button editReportBtn;
	@FXML
	private Button searchBtn;
	@FXML
	private Button openAdminToolBtn;

	/**
	 * Initializes the controller class.
	 */
	@Override
	public void initialize(URL url, ResourceBundle rb) {

		Image newReport = new Image(PsychReports.class.getResourceAsStream("icons/new-report.png"));
		startNewReportBtn.setGraphic(new ImageView(newReport));
		
		Image editReport = new Image(PsychReports.class.getResourceAsStream("icons/edit-report.png"));
		editReportBtn.setGraphic(new ImageView(editReport));
		
		Image searchReport = new Image(PsychReports.class.getResourceAsStream("icons/search-reports.png"));
		searchBtn.setGraphic(new ImageView(searchReport));
	}
	
	@FXML
	protected void openAdminTool() {
		
		try {
			
			State.setState(States.NONE);
			ViewChanger.changeView(Views.REPORT_CREATOR_SCREEN_NAME);
		} catch (IOException e) {
			
			log.error("Unable to go to report creator: " + PsychReportUtils.getStackTrace(e));
		}
	}

	@FXML
	protected void startNewReport() {

		try {
			
			State.setState(States.NEW);
			ViewChanger.changeView(Views.NEW_EDIT_SCREEN_NAME);
		} catch (IOException e) {
			
			log.error("Unable to go to new report: " + PsychReportUtils.getStackTrace(e));
		}
	}

	@FXML
	protected void continueEditReport() {

		try {
			
			State.setState(States.EDIT);
			ViewChanger.changeView(Views.NEW_EDIT_SCREEN_NAME);
		} catch (IOException e) {
			
			log.error("Unable to edit report: " + PsychReportUtils.getStackTrace(e));
		}
	}

	@FXML
	protected void searchReports() {
		
		try {
			
			State.setState(States.NONE);
			ViewChanger.changeView(Views.SEARCH_SCREEN_NAME);
		} catch (IOException e) {
			
			log.error("Unable to go to search report screen: " + PsychReportUtils.getStackTrace(e));
		}
	}

	@FXML
	protected void editProperties() {

		try {
			
			State.setState(States.NONE);
			ViewChanger.changeView(Views.EDIT_PROPERTIES_SCREEN_NAME, 300.0, 125.0);
		} catch (IOException e) {
			
			log.error("Unable to go to properties screen: " + PsychReportUtils.getStackTrace(e));
		}
	}

	@FXML
	protected void exitApplication() {

		System.out.println("Close Application");
		if (PsychReports.cleanUpApplication()) {
			
			System.exit(0);
		}
	}
	
	@FXML
	protected void about() {
		
		try {
			
			ViewChanger.changeView(Views.ABOUT_SCREEN_NAME, 600.0, 400.0);
		} catch (IOException e) {
			
			log.error("Unable to go to properties screen: " + PsychReportUtils.getStackTrace(e));
		}
	}
	
	@FXML
	protected void help() {
		
		
	}
}
