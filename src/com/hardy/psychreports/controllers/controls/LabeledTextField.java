package com.hardy.psychreports.controllers.controls;

import java.io.IOException;

import com.hardy.psychreports.PsychReports;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 725 Sandhill Ct
 * Lehi, Ut. 84043
 * 
 * Description:
 * 
 * Author: Hardy Cherry
 **/
public class LabeledTextField extends HBox {

	@FXML
	protected Label labelLbl;

	@FXML
	protected TextField replacementTxt;
	
	private String tag;

	public LabeledTextField(String tag) {

		this.tag = tag;
		
		FXMLLoader fxmlLoader = new FXMLLoader(PsychReports.class.getResource("views/controls/LabeledTextField.fxml"));
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);

		try {

			fxmlLoader.load();
		} catch (IOException exception) {

			throw new RuntimeException(exception);
		}

		labelLbl.setText(labelLbl.getText().replaceFirst("(0)", this.tag));
	}

	public String getTag() {
		
		return tag;
	}
	
	public String getReplacementText() {

		return replacementTxt.getText();
	}
	
	public void setReplacementText(String text) {
		
		if(text == null) {
			
			text = "";
		}
		
		replacementTxt.setText(text);
	}
}
