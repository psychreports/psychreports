package com.hardy.psychreports.filerepresentation;

import java.util.List;
import java.util.Map;

import javafx.scene.control.Label;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import com.hardy.psychreports.controllers.ViewChanger;
import com.hardy.psychreports.filerepresentation.constants.Font;
import com.hardy.psychreports.filerepresentation.error.XMLObjectUpdateException;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 725 Sandhill Ct
 * Lehi, Ut. 84043
 * 
 * Description: Represents a label for display
 * 
 * Author: Hardy Cherry
 **/
public class XMLLabel extends XMLObject {

	public static final String LABEL_NODE_NAME = "Label";
	public static final String CONTENTS_ATTR = "contents";
	public static final String CONTENTS_WITH_TAGS_ATTR = "contentsWithTags";
	public static final String FONT_SIZE_ATTR = "fontSize";
	
	private String contents;
	private String contentsWithTags;
	private int fontSize;

	public XMLLabel() {

		super();
		contents = "";
		contentsWithTags = "";
		fontSize = Font.DEFAULT_SIZE;
	}

	public String getContents() {

		return contents;
	}

	public void setContents(String contents) {

		this.contents = contents;
	}

	public int getFontSize() {

		return fontSize;
	}

	public void setFontSize(int fontSize) {

		this.fontSize = fontSize;
	}

	@Override
	public void parseXML(Node xml, List<String> errors, Map<String, List<String>> tagsAndIds) {

		if (LABEL_NODE_NAME.equals(xml.getNodeName())) {
		
			NamedNodeMap attrs = xml.getAttributes();
			if(attrs != null) {
				
				try {
					setId(attrs.getNamedItem(ID_ATTR).getNodeValue());
					setContents(attrs.getNamedItem(CONTENTS_ATTR).getNodeValue());
					setFontSize(Integer.parseInt(attrs.getNamedItem(FONT_SIZE_ATTR).getNodeValue()));
				
					try {
						
						contentsWithTags = attrs.getNamedItem(CONTENTS_WITH_TAGS_ATTR).getNodeValue();
					} catch(Exception e) {
						;//if nothing is here just ignore it.
					}
					
					if(getContents() != null && getContents().length() > 0) {
						
						if(contentsWithTags.contains(TAG_START) && contentsWithTags.contains(TAG_END)) {
							
							extractTags(getId(), contentsWithTags, tagsAndIds);
						}
					}
				} catch(Exception e) { 
					
					errors.add(xml.getNodeName() + " : " + e.getMessage());
				}
			}				
		}
	}

	@Override
	public String toXML() {

		StringBuilder result = new StringBuilder();
		result.append("<")
			.append(LABEL_NODE_NAME)
			.append(" ")
			.append(CONTENTS_ATTR)
			.append("=\"")
			.append(getContents())
			.append("\" ")
			.append(CONTENTS_WITH_TAGS_ATTR)
			.append("=\"")
			.append(contentsWithTags)
			.append("\" ")
			.append(ID_ATTR)
			.append("=\"")
			.append(getId())
			.append("\" ")
			.append(FONT_SIZE_ATTR)
			.append("=\"")
			.append(getFontSize())
			.append("\"/>");
	
		return result.toString();
	}

	@Override
	public javafx.scene.Node createSeceneNode() {

		Label label = new Label();
		label.setId(getId());
		label.setManaged(true);
		label.setWrapText(true);
		label.setMaxWidth(ViewChanger.DEFAULT_CONTROL_WIDTH);
		label.setText(getContents());
		label.setFont(new javafx.scene.text.Font(getFontSize()));
		
		return label;
	}

	@Override
	public void clear() {
		
	}
	
	@Override
	public String getNodeName() {

		return LABEL_NODE_NAME;
	}

	@Override
	public void update(Object newValue) throws XMLObjectUpdateException {

		throw new XMLObjectUpdateException("Unable to update XML Object, doesn't support updating.");
	}

	@Override
	public void replaceTags(Map<String, String> tagsAndReplacement) {

		contents = doReplacement(contentsWithTags, tagsAndReplacement);
	}
}
