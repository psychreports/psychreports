package com.hardy.psychreports.daos;

import java.io.File;
import java.text.ParseException;

import org.apache.log4j.Logger;

import com.hardy.psychreports.daos.error.ModelException;
import com.hardy.psychreports.filerepresentation.XMLFile;
import com.hardy.psychreports.filerepresentation.XMLObject;
import com.hardy.psychreports.filerepresentation.error.XMLObjectUpdateException;
import com.hardy.psychreports.utils.PsychReportUtils;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 725 Sandhill Ct
 * Lehi, Ut. 84043
 * 
 * Description:
 * 
 * Author: Hardy Cherry
 **/
public class ReportModel {

	private static Logger log = Logger.getLogger(ReportModel.class);

	private static ReportModel reportModel;
	private XMLFile xmlFile;
	private File templateFile;
	private File saveFile;
	private boolean dirty;

	public static ReportModel getInstance() {

		if (reportModel == null) {

			reportModel = new ReportModel();
		}

		return reportModel;
	}

	private ReportModel() {

		xmlFile = new XMLFile();
		templateFile = null;
		saveFile = null;
	}
	
	public void close() {
		
		reportModel = null;
		xmlFile.clear();
		xmlFile = null;
		templateFile = null;
		saveFile = null;
		dirty = false;
	}

	public void initializeModel(File file) throws ModelException {

		templateFile = file;
		String fileContents = FileAccess.readFile(file);
		xmlFile.clear();
		try {
			xmlFile.parse(fileContents);
		} catch (ParseException e) {
			
			log.error(PsychReportUtils.getStackTrace(e));
			throw new ModelException("Unable to parse provided file");
		}
	}
	
	public void initializeModel(XMLFile xmlFile) throws ModelException {

		this.xmlFile = xmlFile;		
	}
	
	public File getSaveFile() {
		
		return saveFile;
	}
	
	public File getTemplateFile() {
		
		return templateFile;
	}
	
	public void setSavePath(File saveFile) {
		
		this.saveFile = saveFile; 
	}
	
	public void save() throws ModelException {
		
		if(saveFile != null) {
						
			FileAccess.writeFile(saveFile, xmlFile);
			dirty = false;
		} else {
			
			throw new ModelException("Invalid save file. File was null.");
		}
	}
	
	public final XMLFile getXMLFile() {
		
		return xmlFile;
	}
	
	public boolean isDirty() {
		
		return dirty;
	}
	
	public void update(String id, Object value) throws ModelException {
		
		XMLObject obj = xmlFile.getItemById(id);
		
		if(obj != null) {
			
			try {
				
				obj.update(value);
				dirty = true;
			} catch(XMLObjectUpdateException e) {
				
				throw new ModelException(e.getMessage());
			}
		} else {
			
			throw new ModelException("id does not exist.");
		}
	}	
}
