package com.hardy.psychreports.controllers;

import java.net.URL;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

import com.hardy.psychreports.daos.ReportModel;
import com.hardy.psychreports.utils.PsychReportUtils;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Control;
import javafx.scene.layout.Region;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 725 Sandhill Ct
 * Lehi, Ut. 84043
 * 
 * Description:
 * 
 * Author: Hardy Cherry
 **/
public class ReportCreatorController implements Initializable {

	private static Logger log = Logger.getLogger(ReportCreatorController.class);
			
	@Override
	public void initialize(URL location, ResourceBundle resources) {

		// TODO Auto-generated method stub

	}
	
	@FXML
	protected void close() {

		try {
			
			ViewChanger.changeView(Views.START_SCREEN_NAME);
		} catch(Exception e) {
			
			log.error("Unable to go back to start screen: " + PsychReportUtils.getStackTrace(e));
		}
	}
	
	@FXML
	protected void help() {
		
	}
	
	@FXML
	protected void addTable() {
		
	}
	
	@FXML
	protected void addHorizontalGroup() {
		
	}
	
	@FXML
	protected void addCenterGroup() {
		
	}
	
	@FXML
	protected void addVerticalGroup() {
		
	}
	
	@FXML
	protected void addTabGroup() {
		
	}
	
	@FXML
	protected void addCheckboxGroup() {
		
	}
	
	@FXML
	protected void addTextInput() {
		
	}
	
	@FXML
	protected void addTextArea() {
		
	}
	
	@FXML
	protected void addDateField() {
		
	}
	
	@FXML
	protected void addDropDown() {
		
	}
	
	@FXML
	protected void addLabel() {
		
	}
	
	@FXML
	protected void addCheckbox() {
		
	}
	
	private void openReportForEdit() {
		//display a open dialog and call parse report
	}
	
	private void parseReport(ReportModel report) {
		//loop through elements in report and display them in the view area using placeComponent
	}
	
	private void placeComponent(Control toPlace, Node parent) {
		
	}
	
	private void placeComponent(Region toPlace, Node parent) {
		
	}
	
	
}
