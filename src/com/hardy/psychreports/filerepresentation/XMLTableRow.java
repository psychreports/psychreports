package com.hardy.psychreports.filerepresentation;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import com.hardy.psychreports.filerepresentation.error.XMLObjectUpdateException;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 725 Sandhill Ct
 * Lehi, Ut. 84043
 * 
 * Description: Represents a table row
 * 
 * Author: Hardy Cherry
 **/
public class XMLTableRow extends XMLObject {

	public static final String TABLE_ROW_NODE_NAME = "tr";
	public static final String HEADER_ATTR = "header";
	
	private List<XMLTableData> tableData;
	private boolean header;

	public XMLTableRow() {

		super();
		tableData = new ArrayList<>();
		header = false;
	}

	public XMLObject getItemById(String itemId) {
		
		XMLObject result = null;
		
		for(int i = 0; (i < tableData.size()) && (result == null); i++) {
			
			if(itemId.equals(tableData.get(i).getId())) {
				
				result = tableData.get(i);
			} else {
				
				if(tableData.get(i).getData() != null && 
						itemId.equals(tableData.get(i).getData().getId())) {
					
					result = tableData.get(i).getData();
				}
			}
		}
		
		return result;
	}
	
	public boolean replaceItem(String id, XMLTableData xmlObj) {

		boolean replacedItem = false;
		
		for(int i = 0; (i < tableData.size()) && !replacedItem; i++) {
			
			if(id.equals(tableData.get(i).getId())) {
				
				tableData.remove(i);
				tableData.add(i, xmlObj);
				replacedItem = true;
			}
		}
		
		return replacedItem;
	}
	
	public void add(XMLTableData data) {
		
		tableData.add(data);
	}
	
	public List<XMLTableData> getTableData() {

		return tableData;
	}

	public void setTableData(List<XMLTableData> tableData) {

		this.tableData = tableData;
	}
	
	public boolean isHeader() {
		
		return header;
	}
	
	public void setHeader(boolean value) {
		
		header = value;
	}
	
	@Override
	public void parseXML(Node xml, List<String> errors, Map<String, List<String>> tagsAndIds) {

		if (TABLE_ROW_NODE_NAME.equals(xml.getNodeName())) {
		
			NamedNodeMap attrs = xml.getAttributes();
			if(attrs != null) {
				
				try {
					setId(attrs.getNamedItem(ID_ATTR).getNodeValue());	
					if(attrs.getNamedItem(HEADER_ATTR) != null) {
						
						setHeader(Boolean.parseBoolean(attrs.getNamedItem(HEADER_ATTR).getNodeValue()));
					}
				} catch(Exception e) { 
					
					errors.add(xml.getNodeName() + " : " + e.getMessage());
				}
			}		
			
			for(int i = 0; i < xml.getChildNodes().getLength(); i++) {
				
				Node n = xml.getChildNodes().item(i);

				if(XMLTableData.TABLE_DATA_NODE_NAME.equals(n.getNodeName())) {
					
					XMLTableData data = new XMLTableData();
					data.parseXML(n, errors, tagsAndIds);
					tableData.add(data);
				}
			}
		}
	}

	@Override
	public String toXML() {

		StringBuilder result = new StringBuilder();
		result.append("<")
			.append(TABLE_ROW_NODE_NAME)
			.append(" ")
			.append(ID_ATTR)
			.append("=\"")
			.append(getId())
			.append("\" ")
			.append(HEADER_ATTR)
			.append("=\"")
			.append(isHeader())
			.append("\">");
		
		for(int i = 0; i < tableData.size(); i++) {
			
			XMLTableData data = tableData.get(i);
			result.append(data.toXML());
		}
		
		result.append("</")
			.append(TABLE_ROW_NODE_NAME)
			.append(">");
	
		return result.toString();
	}

	@Override
	public javafx.scene.Node createSeceneNode() {

		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void clear() {
		
		if(tableData != null) {
			
			for(int i = 0; i < tableData.size(); i++) {
				
				tableData.get(i).clear();
			}
		}
	}
	
	@Override
	public String getNodeName() {

		return TABLE_ROW_NODE_NAME;
	}
	
	@Override
	public void update(Object newValue) throws XMLObjectUpdateException {
	
		throw new XMLObjectUpdateException("Unable to update XML Object, doesn't support updating.");
	}

	@Override
	public void replaceTags(Map<String, String> tagsAndReplacement) {

		// TODO Auto-generated method stub
	}
}
