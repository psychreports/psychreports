package com.hardy.psychreports.daos.error;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 725 Sandhill Ct
 * Lehi, Ut. 84043
 * 
 * Description:
 * 
 * Author: Hardy Cherry
 **/
public class FileAccessException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5471734449337755132L;

	public FileAccessException(String message) {
		
		super(message);
	}
}
