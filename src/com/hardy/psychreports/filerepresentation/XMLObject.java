package com.hardy.psychreports.filerepresentation;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.w3c.dom.Node;

import com.hardy.psychreports.filerepresentation.error.XMLObjectUpdateException;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 725 Sandhill Ct
 * Lehi, Ut. 84043
 * 
 * Description: the base xml object that all other xml objects should extend.
 * 
 * Author: Hardy Cherry
 **/
public abstract class XMLObject {

	public static final String TAG_START = "{[";
	public static final String TAG_END = "]}";
	
	public static final String ID_ATTR = "id";
	
	private String id;

	public XMLObject() {

		id = "";
	}

	public String getId() {

		return id;
	}

	public void setId(String id) {

		this.id = id;
	}
	
	/**
	 * Takes the textWithTags and searches for any tags, it then adds the tags to the tagsAndIds.
	 * @param id the id of the xmlObj that has textWithTags
	 * @param textWithTags some text that may or may not have tags in it
	 * @param tagsAndIds the map where any tags found should be added.
	 */
	protected void extractTags(String id, String textWithTags, Map<String, List<String>> tagsAndIds) {
		
		int startIndex = 0; 
		int endIndex = 0;
		
		while(textWithTags.indexOf(TAG_START, startIndex) >= startIndex) {
			
			startIndex = textWithTags.indexOf(TAG_START, startIndex);
			endIndex = textWithTags.indexOf(TAG_END, startIndex)+TAG_END.length();
			
			String tag = textWithTags.substring(startIndex, endIndex);
			
			List<String> idsList = null;
			if(tagsAndIds.containsKey(tag)) {
				
				idsList = tagsAndIds.get(tag);
			} else {
				
				idsList = new ArrayList<>();
			}
			//only add id's once, we don't need duplicates.
			if(!idsList.contains(id)) {
				
				idsList.add(id);
			}
			
			tagsAndIds.put(tag, idsList);
			startIndex = endIndex;			
		}
	}
	
	/**
	 * Takes a stringWithTags and loops through all the tags in tagsAndReplacement 
	 * and replaces any found in stringWithTags with the replacement string.
	 * @param stringWithTags the string to remove tags from
	 * @param tagsAndReplacement the map of tags and thier replacements
	 * @return a string with no tags.
	 */
	protected String doReplacement(String stringWithTags, Map<String, String> tagsAndReplacement) {
		
		String result = stringWithTags;
		
		Iterator<String> tagIter = tagsAndReplacement.keySet().iterator();
		while(tagIter.hasNext()) {
			
			String tag = tagIter.next();
			String replacement = tagsAndReplacement.get(tag);
			while(result.contains(tag)) {
				
				tag = tag.replaceAll("\\{", "\\\\{");
				tag = tag.replaceAll("\\}", "\\\\}");
				tag = tag.replaceAll("\\[", "\\\\[");
				tag = tag.replaceAll("\\]", "\\\\]");
				result = result.replaceAll(tag, replacement);
			}
		}
		
		return result;
	}
	
	/**
	 * Takes xml and parses it into the current class.
	 * @param xml the xml node to parse.
	 * @param errors the errors object, all errors that occure will be stored in this
	 * @param tagsAndIds this will hold tags that should be replaced with some other 
	 * text and the ids of the xmlObjs that contain that tag. Tags will be in the
	 * format {[some tag]} spaces are allowed, anything within the {[ ]} will be replaced.
	 * if it's empty after the parse there were no problems.
	 */
	public abstract void parseXML(Node xml, List<String> errors, Map<String, List<String>> tagsAndIds);
	
	/**
	 * @return returns an xml formatted string representing the current class.
	 */
	public abstract String toXML();
	
	/**
	 * Each xml object can create a displayable version of it self.
	 * @return a scene node that can be added to the display.
	 */
	public abstract javafx.scene.Node createSeceneNode();
	
	/**
	 * @return the name of the tag that would be displayed in the xml.
	 */
	public abstract String getNodeName();
	
	/**
	 * Allows you to update the important data stored by the given XML object.
	 * @param newValue the new value to replace the old date with.
	 */
	public abstract void update(Object newValue) throws XMLObjectUpdateException;
	
	/**
	 * Takes the tagsAndReplacement and replaces any tags in this object with 
	 * the repalcement text in the map.
	 * @param tagsAndReplacement the tags to look for and the text to replace them with.
	 */
	public abstract void replaceTags(Map<String, String> tagsAndReplacement);
	
	/**
	 * This method should clean up anything that could leave a memory leak when
	 * the object is no longer in use.
	 */
	public abstract void clear();
}
