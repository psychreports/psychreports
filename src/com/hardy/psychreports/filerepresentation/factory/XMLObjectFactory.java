package com.hardy.psychreports.filerepresentation.factory;

import com.hardy.psychreports.filerepresentation.XMLCheckbox;
import com.hardy.psychreports.filerepresentation.XMLDateField;
import com.hardy.psychreports.filerepresentation.XMLDropDown;
import com.hardy.psychreports.filerepresentation.XMLLabel;
import com.hardy.psychreports.filerepresentation.XMLObject;
import com.hardy.psychreports.filerepresentation.groups.XMLTab;
import com.hardy.psychreports.filerepresentation.XMLTable;
import com.hardy.psychreports.filerepresentation.XMLTableData;
import com.hardy.psychreports.filerepresentation.XMLTableRow;
import com.hardy.psychreports.filerepresentation.XMLTextArea;
import com.hardy.psychreports.filerepresentation.XMLTextBox;
import com.hardy.psychreports.filerepresentation.error.XMLObjectCreationException;
import com.hardy.psychreports.filerepresentation.groups.XMLCenterGroup;
import com.hardy.psychreports.filerepresentation.groups.XMLCheckboxGroup;
import com.hardy.psychreports.filerepresentation.groups.XMLGroup;
import com.hardy.psychreports.filerepresentation.groups.XMLHorizontalGroup;
import com.hardy.psychreports.filerepresentation.groups.XMLTabPane;
import com.hardy.psychreports.filerepresentation.groups.XMLVerticalGroup;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 725 Sandhill Ct
 * Lehi, Ut. 84043
 * 
 * Description: Creates XMLObjects based on the node name.
 * 
 * Author: Hardy Cherry
 **/
public class XMLObjectFactory {

	public static XMLObject getInstance(String nodeName) throws XMLObjectCreationException {

		XMLObject object = null;

		switch (nodeName) {
			case XMLCheckbox.CHECKBOX_NODE_NAME:
				object = new XMLCheckbox();
				break;
			case XMLDateField.DATEFIELD_NODE_NAME:
				object = new XMLDateField();
				break;
			case XMLLabel.LABEL_NODE_NAME:
				object = new XMLLabel();
				break;
			case XMLTable.TABLE_NODE_NAME:
				object = new XMLTable();
				break;
			case XMLTableData.TABLE_DATA_NODE_NAME:
				object = new XMLTableData();
				break;
			case XMLTableRow.TABLE_ROW_NODE_NAME:
				object = new XMLTableRow();
				break;
			case XMLTextArea.TEXT_AREA_NODE_NAME:
				object = new XMLTextArea();
				break;
			case XMLTextBox.TEXT_BOX_NODE_NAME:
				object = new XMLTextBox();
				break;
			case XMLCenterGroup.CENTERGROUP_NODE_NAME:
				object = new XMLCenterGroup();
				break;
			case XMLCheckboxGroup.CHECKBOXGROUP_NODE_NAME:
				object = new XMLCheckboxGroup();
				break;
			case XMLGroup.GROUP_NODE_NAME:
				object = new XMLGroup();
				break;
			case XMLHorizontalGroup.HORIZONTALGROUP_NODE_NAME:
				object = new XMLHorizontalGroup();
				break;
			case XMLVerticalGroup.VERTICALGROUP_NODE_NAME:
				object = new XMLVerticalGroup();
				break;
			case XMLTabPane.TABPANE_NODE_NAME:
				object = new XMLTabPane();
				break;
			case XMLDropDown.DROPDOWN_NODE_NAME:
				object = new XMLDropDown();
				break;
			case XMLTab.TAB_NODE_NAME:
				object = new XMLTab();
				break;
			case "#text":
				break;
			default:
				throw new XMLObjectCreationException("Couldn't parse xml.");
		}

		return object;
	}
}
