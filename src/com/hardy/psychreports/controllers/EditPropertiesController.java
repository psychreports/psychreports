package com.hardy.psychreports.controllers;

import java.net.URL;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

import com.hardy.psychreports.daos.SettingsModel;
import com.hardy.psychreports.utils.PsychReportUtils;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 725 Sandhill Ct
 * Lehi, Ut. 84043
 * 
 * Description:
 * 
 * Author: Hardy Cherry
 **/
public class EditPropertiesController implements Initializable {

	private static Logger log = Logger.getLogger(EditPropertiesController.class);
			
	public TextField locationTxt;
		
	@Override
	public void initialize(URL location, ResourceBundle resources) {

		SettingsModel model = SettingsModel.getInstance();
		String reportDir = model.getReportDirectory();
		locationTxt.setText(reportDir);		
	}
	
	@FXML
	protected void cancel() {
		
		try {
			
			ViewChanger.changeView(Views.START_SCREEN_NAME);
		} catch(Exception e) {
			
			log.error("Unable to cancel and switch to startScreen: " + PsychReportUtils.getStackTrace(e));
		}
	}
	
	@FXML
	protected void save() {
		
		try {
			
			ViewChanger.changeView(Views.START_SCREEN_NAME);
		} catch(Exception e) {
			
			log.error("Unable to save and switch to start screen: " + PsychReportUtils.getStackTrace(e));
		}
	}
	
	@FXML
	protected void browse() {
		
		
	}


}
