package com.hardy.psychreports.filerepresentation.error;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 725 Sandhill Ct
 * Lehi, Ut. 84043
 * 
 * Description: exception class
 * 
 * Author: Hardy Cherry
 **/
public class XMLObjectCreationException extends Exception {

	private static final long serialVersionUID = 1L;

	public XMLObjectCreationException(String message) {
		
		super(message);
	}
}
